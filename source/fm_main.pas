{
  Copyright 2020-2021 Riva

  FreeBSD License, modified (see modification at the end)
  ================================================================================
  https://opensource.org/licenses/BSD-2-Clause

  Redistribution and use in source and binary forms, with or without modification,
  are permitted provided that the following conditions are met:

  1.  Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.

  2.  Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
  THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  --------------------------------------------------------------------------------

  License modification:
  You can not sell this software product (including sources and binaries)
  without copyright holder's permission.
}

unit fm_main;

{$mode objfpc}{$H+}

interface

uses
  Classes, Forms, SysUtils, FileUtil, FileCtrl, ExtCtrls, Grids, Buttons,
  Controls, Graphics, Dialogs, Menus, ActnList, ComCtrls, StdCtrls,
  LazFileUtils, LazUTF8, IniPropStorage, LCLTranslator,

  fm_port, fm_confirm, fm_settings, fm_about,  // формы
  u_CasioFile, u_comm, u_utilities, u_strings; // модули

const
  CONNECT_TIMEOUT = 200;  // [ms] таймаут подключения к порту
  SYSTEM_TIMER    = 50;   // [ms] период системного таймера

type

  { TfmMain }

  TfmMain = class(TForm)
    acFileDirSelect:  TAction;
    acFileOpen:       TAction;
    acFileSave:       TAction;
    acFileSaveAs:     TAction;
    acExit:           TAction;
    acFile:           TAction;
    acComTxOne:       TAction;
    acComTxAll:       TAction;
    acComConnect:     TAction;
    acCom:            TAction;
    acComParameters:  TAction;
    acFilePropShow:   TAction;
    acProgPropShow:   TAction;
    acProg:           TAction;
    acProgCopy:       TAction;
    acProgCut:        TAction;
    acProgPaste:      TAction;
    acProgRemove:     TAction;
    acProgRemoveAll:  TAction;
    acFileDirOpen:    TAction;
    acFileClose:      TAction;
    acAbout:          TAction;
    acHelp:           TAction;
    acFileRemove:     TAction;
    acMenuShow:       TAction;
    acSettings:       TAction;
    acComErrorClose:  TAction;
    acVisitSite:      TAction;
    AppActionList:    TActionList;
    AppProperties:    TApplicationProperties;
    bbComError:       TBitBtn;
    edCatalog:        TEdit;
    fbFiles:          TFileListBox;
    imA16:            TImageList;
    imD16:            TImageList;
    IniStorageMain:   TIniPropStorage;
    lbFileProperties: TLabel;
    lbFTypeTxt:       TLabel;
    lbFSizeTxt:       TLabel;
    lbFItemsTxt:      TLabel;
    lbFSizePTxt:      TLabel;
    lbPIndexTxt:      TLabel;
    lbPCommentTxt:    TLabel;
    lbPSizeTxt:       TLabel;
    lbPModeTxt:       TLabel;
    lbPSDataTxt:      TLabel;
    lbPSGraphTxt:     TLabel;
    lbProgProperties: TLabel;
    lbComErrorHelp:   TLabel;
    lbSite:           TLabel;
    lbShortHelp:      TLabel;
    lbPMode:          TLabel;
    lbFName:          TLabel;
    lbPIndex:         TLabel;
    lbPSize:          TLabel;
    lbPSData:         TLabel;
    lbPSGraph:        TLabel;
    lbFType:          TLabel;
    lbFSize:          TLabel;
    lbFItems:         TLabel;
    lbFSizeP:         TLabel;
    lbFNameTxt:       TLabel;
    lbPComment:       TLabel;
    lbProgress:       TLabel;
    lbProgressText:   TLabel;
    AppMainMenu:      TMainMenu;
    MenuItem14:       TMenuItem;
    MenuItem15:       TMenuItem;
    MenuItem16:       TMenuItem;
    MenuItem17:       TMenuItem;
    MenuItem19:       TMenuItem;
    MenuItem20:       TMenuItem;
    MenuItem21:       TMenuItem;
    MenuItem22:       TMenuItem;
    MenuItem23:       TMenuItem;
    MenuItem24:       TMenuItem;
    MenuItem25:       TMenuItem;
    MenuItem26:       TMenuItem;
    MenuItem27:       TMenuItem;
    MenuItem28:       TMenuItem;
    MenuItem29:       TMenuItem;
    MenuItem30:       TMenuItem;
    MenuItem31:       TMenuItem;
    MenuItem32:       TMenuItem;
    MenuItem33:       TMenuItem;
    MenuItem1:        TMenuItem;
    MenuItem10:       TMenuItem;
    MenuItem11:       TMenuItem;
    MenuItem12:       TMenuItem;
    MenuItem13:       TMenuItem;
    MenuItem18:       TMenuItem;
    MenuItem2:        TMenuItem;
    MenuItem3:        TMenuItem;
    MenuItem4:        TMenuItem;
    MenuItem5:        TMenuItem;
    MenuItem6:        TMenuItem;
    MenuItem7:        TMenuItem;
    MenuItem8:        TMenuItem;
    MenuItem9:        TMenuItem;
    mmPreview:        TMemo;
    pFilePropTable:   TPanel;
    pProgPropTable:   TPanel;
    pShortInfo:       TPanel;
    pCatalog:         TPanel;
    pExplorer:        TPanel;
    pMain:            TPanel;
    pPreview:         TPanel;
    pFileProperties:  TPanel;
    pProgProperties:  TPanel;
    pbProgress:       TProgressBar;
    dlgSave:          TSaveDialog;
    dlgSelectDir:     TSelectDirectoryDialog;
    pProgress:        TPanel;
    spHorizontal:     TSplitter;
    sbStatusBar:      TStatusBar;
    sgPrograms:       TStringGrid;
    spVertical:       TSplitter;
    TimerMain:        TTimer;
    tbFiles:          TToolBar;
    tbMain:           TToolBar;
    ToolButton1:      TToolButton;
    ToolButton10:     TToolButton;
    ToolButton11:     TToolButton;
    ToolButton12:     TToolButton;
    ToolButton13:     TToolButton;
    ToolButton14:     TToolButton;
    ToolButton15:     TToolButton;
    ToolButton16:     TToolButton;
    ToolButton17:     TToolButton;
    ToolButton18:     TToolButton;
    ToolButton19:     TToolButton;
    ToolButton2:      TToolButton;
    ToolButton20:     TToolButton;
    ToolButton21:     TToolButton;
    ToolButton22:     TToolButton;
    ToolButton23:     TToolButton;
    ToolButton24:     TToolButton;
    ToolButton25:     TToolButton;
    ToolButton27:     TToolButton;
    ToolButton3:      TToolButton;
    ToolButton4:      TToolButton;
    ToolButton5:      TToolButton;
    ToolButton6:      TToolButton;
    ToolButton7:      TToolButton;
    ToolButton8:      TToolButton;
    ToolButton9:      TToolButton;
    procedure acAboutExecute(Sender: TObject);
    procedure acComConnectExecute(Sender: TObject);
    procedure acComErrorCloseExecute(Sender: TObject);
    procedure acComParametersExecute(Sender: TObject);
    procedure acComTxAllExecute(Sender: TObject);
    procedure acComTxOneExecute(Sender: TObject);
    procedure acExitExecute(Sender: TObject);
    procedure acFileCloseExecute(Sender: TObject);
    procedure acFileDirOpenExecute(Sender: TObject);
    procedure acFileDirSelectExecute(Sender: TObject);
    procedure acFileOpenExecute(Sender: TObject);
    procedure acFileRemoveExecute(Sender: TObject);
    procedure acFileSaveAsExecute(Sender: TObject);
    procedure acFileSaveExecute(Sender: TObject);
    procedure acProgCopyExecute(Sender: TObject);
    procedure acProgCutExecute(Sender: TObject);
    procedure acProgPasteExecute(Sender: TObject);
    procedure acProgRemoveAllExecute(Sender: TObject);
    procedure acProgRemoveExecute(Sender: TObject);
    procedure acSettingsExecute(Sender: TObject);
    procedure acVisitSiteExecute(Sender: TObject);
    procedure fbFilesKeyPress(Sender: TObject; var Key: Char);
    procedure FormChangeBounds(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure FormCreate(Sender: TObject);
    procedure FormDropFiles(Sender: TObject; const FileNames: array of String);
    procedure FormShow(Sender: TObject);
    procedure LoadSettingsFromIni;
    procedure IniStorageMainSaveProperties(Sender: TObject);
    procedure sgProgramsChangeBounds(Sender: TObject);
    procedure sgProgramsSelectCell(Sender: TObject; aCol, aRow: Integer;
      var CanSelect: Boolean);
    procedure TimerMainTimer(Sender: TObject);
  PRIVATE
    procedure OnCommRxStart;
    procedure OnCommReceiving;
    procedure OnCommRxEnd;
    procedure OnCommTxStart;
    procedure OnCommTransmiting;
    procedure OnCommTxEnd;

    procedure UpdateFileProperties;
    procedure UpdateProgramProperties;
    procedure UpdateProgramsList;
    procedure UpdateStatusBar;

    function DialogSaveFileDone: Boolean;
    procedure SettingsApply;
    procedure LanguageChange;
  PUBLIC
    { public declarations }
  end;


var
  fmMain:  TfmMain;
  comm:    TCasioCommThread;
  cfile:   TCasioFile;
  portCnt: Integer = CONNECT_TIMEOUT div SYSTEM_TIMER;
  dbg:     String;

implementation

{$R *.lfm}

{ TfmMain }

procedure TfmMain.FormCreate(Sender: TObject);
  begin
    comm.Start;

    comm.OnRxStart     := @OnCommRxStart;
    comm.OnReceiving   := @OnCommReceiving;
    comm.OnRxEnd       := @OnCommRxEnd;
    comm.OnTxStart     := @OnCommTxStart;
    comm.OnTransmiting := @OnCommTransmiting;
    comm.OnTxEnd       := @OnCommTxEnd;

    mmPreview.Align            := alClient;
    IniStorageMain.IniFileName := ExtractFileDir(ParamStrUTF8(0)) + SETTINGS_FILE;

    sbStatusBar.Panels.Items[2].Width := Canvas.GetTextWidth('0') * 23;
  end;

procedure TfmMain.FormShow(Sender: TObject);
  var
    s: String;
  begin
    LoadSettingsFromIni;

    // загрузка настроек порта и автоподключение (согласно настройке)
    with fmPort do comm.PortSettings(Port, Baud, Parity);
    comm.Started := fmSettings.PortAutoConnect;

    // обработка открытия каталога/файла через параметры запуска
    if ParamStrUTF8(1) <> '' then
      FormDropFiles(Sender, ParamStrUTF8(1));

    TimerMain.Interval := SYSTEM_TIMER;
    lbSite.Visible     := fmAbout.AppSiteAvailable;
    lbSite.Caption     := fmAbout.AppSite;

    UpdateProgramsList;
    UpdateFileProperties;
    UpdateProgramProperties;

    fmAbout.ShowSplash(fmSettings.ShowSplash);

    // проверка доступности портов в системе
    if fmSettings.ShowPortWarning and (fmPort.PortsCount = 0) then
      fmConfirm.Show(TXT_WARNING, MultiString(TXT_NOPORTS), [mbYes], Self);
  end;

procedure TfmMain.FormDropFiles(Sender: TObject; const FileNames: array of String);
  begin
    if FileExistsUTF8(FileNames[0]) then
      edCatalog.Text := ExtractFileDir(FileNames[0]) else
      edCatalog.Text := FileNames[0];

    fbFiles.Directory := edCatalog.Text;
    { если бросать файл впервые после запуска, то он не открывается,
      поэтому для устранения такого поведения поставлена короткая задержка }
    Sleep(20);
    fbFiles.ItemIndex := fbFiles.Items.IndexOf(ExtractFileName(FileNames[0]));

    if fbFiles.ItemIndex >= 0 then
      begin
      fbFiles.Click;               // это нужно для обновления поля fbFiles.FileName
      Application.ProcessMessages; // обновление состояния
      acFileOpen.Execute;          // открываем файл
      end;
  end;

procedure TfmMain.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
  begin
    CanClose := True;
    if cfile.Changed then
      CanClose := fmConfirm.Show(TXT_CONFIRM, MultiString(WARN_APP_CLOSE),
        [mbYes, mbNo], Self) = mrYes;
  end;

procedure TfmMain.FormChangeBounds(Sender: TObject);
  begin
    with sbStatusBar.Panels do
      Items[0].Width := fmMain.Width - sbStatusBar.Height - Items[1].Width - Items[2].Width;

    if spVertical.Left < pExplorer.Constraints.MinWidth then
      spVertical.Left := pExplorer.Constraints.MinWidth;

    if fmMain.Width - spVertical.Left < pPreview.Constraints.MinWidth then
      spVertical.Left := fmMain.Width - pPreview.Constraints.MinWidth;
  end;

procedure TfmMain.TimerMainTimer(Sender: TObject);
  var
    s: String;
  begin
    // таймаут по ошибке подключения
    if comm.Connected then
      portCnt := CONNECT_TIMEOUT div SYSTEM_TIMER else
      if portCnt > 0 then
        portCnt      -= 1  else
        comm.Started := False;

    // управление доступностью элементов интерфейса пользователя
    pFileProperties.Visible := acFilePropShow.Checked;
    pProgProperties.Visible := acProgPropShow.Checked;
    acFileSave.Enabled      := cfile.IsValid and cfile.Changed;
    acFileSaveAs.Enabled    := cfile.IsValid;
    acFileOpen.Enabled      := (fbFiles.Items.Count > 0) and (fbFiles.SelCount > 0);
    acFileRemove.Enabled    := acFileOpen.Enabled;
    acFileClose.Enabled     := cfile.IsValid;
    acComConnect.Caption    := BoolToStr(comm.Started, TXT_DISCONNECT, TXT_CONNECT);
    acComConnect.ImageIndex := Integer(CheckBoolean(comm.Started, 18, 19));
    acComTxOne.Enabled      := cfile.IsValid and (comm.Status = csOnline);
    acComTxAll.Enabled      := acComTxOne.Enabled and not cfile.IsSingle;
    acProgRemove.Enabled    := cfile.IsValid;
    acProgRemoveAll.Enabled := cfile.IsValid;
    acProgCopy.Enabled      := cfile.IsValid;
    acProgCut.Enabled       := cfile.IsValid;
    acProgPaste.Enabled     := cfile.IsValid and cfile.IsClipboardFull;

    // обработка видимости элементов окна предпросмотра и прогресса приема/передачи
    if comm.Error <> ceNone then
      begin
      acComErrorClose.Visible   := True;
      lbComErrorHelp.Visible    := True;
      lbProgressText.Caption    := comm.ErrorString;
      lbProgressText.Font.Color := clRed;
      end
    else
      begin
      acComErrorClose.Visible   := False;
      lbComErrorHelp.Visible    := False;
      lbProgressText.Font.Color := clDefault;

      pProgress.Visible := (comm.Status in [csRxing, csTxing]);
      mmPreview.Visible := cfile.IsValid and not pProgress.Visible;
      end;

    // заголовок приложения, имя текущего открытого файла
    s := fmAbout.AppIntName;
    if (cfile.FileName <> '') or cfile.Changed then
      begin
      s += ' - [';
      if cfile.Changed then s += '*';
      s += ExtractFileName(cfile.FileName) + ']';
      end;
    Caption           := s;
    Application.Title := s;

    UpdateStatusBar;
  end;


procedure TfmMain.sgProgramsSelectCell(Sender: TObject; aCol, aRow: Integer;
  var CanSelect: Boolean);
  begin
    if cfile.IsValid then
      begin
      cfile.Current     := aRow;
      mmPreview.Caption :=
        WinCPToUTF8(cfile.Preview(-1, LowerCase(mmPreview.Font.Name) = 'casiodoc'));
      UpdateProgramProperties;
      end;
  end;

procedure TfmMain.sgProgramsChangeBounds(Sender: TObject);
  const
    w: array [0..4] of Integer = (13, 13, 19, 7, 7); // %
  var
    x, a, i: Integer;
  begin
    if sgPrograms.Visible and (sgPrograms.RowCount > 0) then
      begin
      a := sgPrograms.Width - VertScrollBar.Size;
      sgPrograms.ColWidths[0] := w[0] * a div 100;
      sgPrograms.ColWidths[2] := w[1] * a div 100;
      sgPrograms.ColWidths[3] := w[2] * a div 100;
      sgPrograms.ColWidths[4] := w[3] * a div 100;
      sgPrograms.ColWidths[5] := w[4] * a div 100;

      x := 0;
      for i := 0 to 4 do
        x += w[i] * a div 100;

      sgPrograms.ColWidths[1] := a - x;
      end;
  end;

procedure TfmMain.fbFilesKeyPress(Sender: TObject; var Key: Char);
  begin
    if Ord(Key) = 13 then
      acFileOpen.Execute;
  end;


procedure TfmMain.UpdateFileProperties;
  begin
    if cfile.IsValid then
      begin
      lbFName.Caption  := ExtractFileNameOnly(cfile.FileName);
      lbFType.Caption  := cfile.FileType;
      lbFSize.Caption  := FileSize(cfile.FileName).ToString;
      lbFItems.Caption := cfile.ItemsCount.ToString;
      lbFSizeP.Caption := cfile.ItemsSize.ToString;
      end
    else
      begin
      lbFName.Caption  := ' ';
      lbFType.Caption  := '';
      lbFSize.Caption  := '';
      lbFItems.Caption := '';
      lbFSizeP.Caption := '';
      end;
  end;

procedure TfmMain.UpdateProgramProperties;
  begin
    if cfile.IsValid then
      begin
      lbPIndex.Caption   := cfile.Info.Name;
      lbPComment.Caption := cfile.Info.Comment;
      lbPSize.Caption    := (cfile.Info.Size - 1).ToString;
      lbPMode.Caption    := TxtCasioProgMode[cfile.Info.Mode];
      lbPSData.Caption   := TxtCasioSDataMode[cfile.Info.SData];
      lbPSGraph.Caption  := TxtCasioSGraphMode[cfile.Info.SGraph];
      end
    else
      begin
      lbPIndex.Caption   := ' ';
      lbPComment.Caption := '';
      lbPSize.Caption    := '';
      lbPMode.Caption    := '';
      lbPSData.Caption   := '';
      lbPSGraph.Caption  := '';
      end;
  end;

procedure TfmMain.UpdateProgramsList;
  var
    i: Integer;
  begin
    mmPreview.Clear;
    sgPrograms.RowCount  := 0; // обнуляем, чтобы сбросить табличный курсор
    sgPrograms.Visible   := cfile.IsValid and not cfile.IsSingle;
    spHorizontal.Visible := cfile.IsValid and not cfile.IsSingle;

    if cfile.IsValid and not cfile.IsSingle then
      begin
      fbFiles.AnchorSideTop.Control := spHorizontal;
      fbFiles.AnchorSideTop.Side    := asrBottom;
      sgPrograms.RowCount           := CASIO_PROGS;
      end
    else
      begin
      fbFiles.AnchorSideTop.Control := pExplorer;
      fbFiles.AnchorSideTop.Side    := asrTop;
      sgPrograms.RowCount           := 1;
      end;

    if cfile.IsValid then
      for i := 0 to sgPrograms.RowCount - 1 do
        with sgPrograms.Rows[i] do
          begin
          Strings[0] := cfile.Info(i).Name;
          Strings[3] := TxtCasioProgMode[cfile.Info(i).Mode];
          Strings[4] := TxtCasioSDataMode[cfile.Info(i).SData][1];
          Strings[5] := TxtCasioSGraphMode[cfile.Info(i).SGraph][1];
          if cfile.Info(i).Size = 1 then
            begin
            Strings[1] := TXT_CELL_EMPTY;
            Strings[2] := '';
            end
          else
            begin
            Strings[1] := cfile.Info(i).Comment;
            Strings[2] := (cfile.Info(i).Size - 1).ToString;
            end;
          end;
  end;

procedure TfmMain.UpdateStatusBar;
  begin
    with sbStatusBar.Panels do
      begin
      Items[1].Text := comm.StatusString;
      Items[2].Text := comm.Port + ' / ' + comm.Parity + ' / '
        + comm.Baudrate.ToString + ' ' + TXT_BAUDRATE;
      end;
  end;


function TfmMain.DialogSaveFileDone: Boolean;
  begin
    Result := False;

    dlgSave.FileName   := ExtractFileName(cfile.FileName);
    dlgSave.InitialDir := ExtractFileDir(cfile.FileName);

    if dlgSave.Execute then
      begin
      cfile.SaveToFile(dlgSave.FileName);
      fbFiles.UpdateFileList;
      UpdateFileProperties;
      Result := True;
      end;
  end;

procedure TfmMain.SettingsApply;
  begin
    if fmSettings.ShowMenu then
      fmMain.Menu := AppMainMenu  else
      fmMain.Menu := nil;

    edCatalog.Visible  := fmSettings.ShowCatalog;
    mmPreview.WordWrap := fmSettings.PreviewWrap;

    mmPreview.Font.Assign(fmSettings.FontView);
    fbFiles.Font.Assign(fmSettings.FontExplorer);
    sgPrograms.Font.Assign(fmSettings.FontExplorer);

    sgPrograms.DefaultRowHeight := sgPrograms.Canvas.GetTextHeight('0') * 150 div 100;
    fbFiles.ItemHeight          := sgPrograms.DefaultRowHeight;

    LanguageChange;

    // хитрый способ обновить вывод предпросмотра
    sgPrograms.Tag := sgPrograms.Row;
    sgPrograms.Row := -1;
    sgPrograms.Row := sgPrograms.Tag;
  end;

procedure TfmMain.LanguageChange;
  begin
    SetDefaultLang(fmSettings.Language, ExtractFileDir(ParamStrUTF8(0)) + LANGUAGES_DIR);

    fmAbout.UpdateInfo; // обновляем инфо на новом языке

    // здесь обновляются многострочные метки
    acComConnect.Hint      := MultiString(HINT_CONNECT);
    lbComErrorHelp.Caption := MultiString(HINT_ERROR);
    lbShortHelp.Caption    := fmAbout.AppDescription + LineEnding + LineEnding
      + MultiString(HINT_MAIN) + LineEnding + LineEnding
      + fmAbout.AppBrief + LineEnding + fmAbout.AppCopyright;
  end;

procedure TfmMain.IniStorageMainSaveProperties(Sender: TObject);
  begin
    IniStorageMain.IniSection := 'Last State';

    IniStorageMain.WriteString('WorkDir', fbFiles.Directory);

    IniStorageMain.WriteInteger('WindowTop', fmMain.Top);
    IniStorageMain.WriteInteger('WindowLeft', fmMain.Left);
    IniStorageMain.WriteInteger('WindowWidth', fmMain.Width);
    IniStorageMain.WriteInteger('WindowHeight', fmMain.Height);

    IniStorageMain.WriteBoolean('FileProp', acFilePropShow.Checked);
    IniStorageMain.WriteBoolean('ProgProp', acProgPropShow.Checked);

    IniStorageMain.WriteInteger('SplitterVert', spVertical.Left);
    IniStorageMain.WriteInteger('SplitterHorz', spHorizontal.Top);

    fmPort.SaveSettingsToIni(IniStorageMain);
  end;

procedure TfmMain.LoadSettingsFromIni;
  begin
    fmPort.LoadSettingsFromIni(IniStorageMain);
    fmSettings.LoadFieldsFromComponents;
    SettingsApply;

    IniStorageMain.IniSection := 'Last State';

    FormDropFiles(nil, IniStorageMain.ReadString('WorkDir', ExtractFileDir(ParamStrUTF8(0))));

    fmMain.Top    := IniStorageMain.ReadInteger('WindowTop', fmMain.Top);
    fmMain.Left   := IniStorageMain.ReadInteger('WindowLeft', fmMain.Left);
    fmMain.Width  := IniStorageMain.ReadInteger('WindowWidth', fmMain.Width);
    fmMain.Height := IniStorageMain.ReadInteger('WindowHeight', fmMain.Height);

    acFilePropShow.Checked := IniStorageMain.ReadBoolean('FileProp', False);
    acProgPropShow.Checked := IniStorageMain.ReadBoolean('ProgProp', False);
    TimerMainTimer(nil);

    spVertical.Left  := IniStorageMain.ReadInteger('SplitterVert', spVertical.Left);
    spHorizontal.Top := IniStorageMain.ReadInteger('SplitterHorz', spHorizontal.Top);
  end;


{ ***  События класса-коммуникатора  *** }

procedure TfmMain.OnCommRxStart;
  begin
    pbProgress.Position    := 0;
    lbProgressText.Caption := TXT_RXING;
  end;

procedure TfmMain.OnCommRxEnd;
  begin
    if comm.CasioFile.IsValid then
      begin
      if (cfile.IsValid or cfile.Changed) and fmSettings.RxRewriteWarning then
        if (fmConfirm.Show(TXT_CONFIRM, MultiString(WARN_REWRITE),
          [mbYes, mbNo], Self) <> mrYes) then Exit;

      cfile.Raw := comm.CasioFile.Raw;

      UpdateProgramsList;
      UpdateFileProperties;

      cfile.Current  := 0;
      sgPrograms.Row := cfile.Current;
      fmMain.ShowOnTop;
      end;
  end;

procedure TfmMain.OnCommReceiving;
  begin
    pbProgress.Position := comm.ProgressPercent;
    lbProgress.Caption  := comm.ProgressPercent.ToString + ' %';
  end;

procedure TfmMain.OnCommTxStart;
  begin
    pbProgress.Position    := 0;
    lbProgressText.Caption := TXT_TXING;
  end;

procedure TfmMain.OnCommTxEnd;
  begin
  end;

procedure TfmMain.OnCommTransmiting;
  begin
    pbProgress.Position := comm.ProgressPercent;
    lbProgress.Caption  := comm.ProgressPercent.ToString + ' %';
  end;


{ ==== обработчики действий группы <Связь> / acCom[Action] ==== }

procedure TfmMain.acComParametersExecute(Sender: TObject);
  var
    tmp: Boolean;
  begin
    tmp          := comm.Started;
    comm.Started := False;

    with fmPort do
      if ShowModal = mrOk then
        comm.PortSettings(Port, Baud, Parity);

    comm.Started := tmp;
    portCnt      := CONNECT_TIMEOUT div SYSTEM_TIMER;
  end;

procedure TfmMain.acComConnectExecute(Sender: TObject);
  begin
    comm.Started := not comm.Started;
    portCnt      := CONNECT_TIMEOUT div SYSTEM_TIMER;
  end;

procedure TfmMain.acComErrorCloseExecute(Sender: TObject);
  begin
    comm.ResetError;
  end;

procedure TfmMain.acComTxAllExecute(Sender: TObject);
  begin
    comm.CasioFile.Raw          := cfile.Raw;
    comm.CasioFile.TransferMode := tmSet;
    comm.Transmit;
  end;

procedure TfmMain.acComTxOneExecute(Sender: TObject);
  begin
    comm.CasioFile.Raw          := cfile.Raw;
    comm.CasioFile.Current      := cfile.Current;
    comm.CasioFile.TransferMode := tmSingle;
    comm.Transmit;
  end;


{ ==== обработчики действий группы <Файл> / acFile[Action] ==== }

procedure TfmMain.acFileDirOpenExecute(Sender: TObject);
  begin
    RootOpenInExplorer(fbFiles.Directory);
  end;

procedure TfmMain.acFileDirSelectExecute(Sender: TObject);
  begin
    if dlgSelectDir.Execute then
      begin
      edCatalog.Text    := dlgSelectDir.FileName;
      fbFiles.Directory := edCatalog.Text;
      end;
  end;

procedure TfmMain.acFileOpenExecute(Sender: TObject);
  var
    tmp: TCasioFile;
  begin
    if cfile.Changed then
      if (fmConfirm.Show(TXT_CONFIRM, MultiString(WARN_NOTSAVED),
        [mbYes, mbNo], Self) <> mrYes) then Exit;

    // загрузка файла с предварительной проверкой корректности формата и целостности
    tmp := TCasioFile.Create;
    if tmp.LoadFromFile(fbFiles.FileName) then
      begin
      cfile.LoadFromFile(fbFiles.FileName);
      comm.ResetError;
      end
    else
      if fmSettings.ShowFormatError then
        fmConfirm.Show(TXT_WARNING, MultiString(WARN_INVALID), [mbCancel], Self);
    tmp.Free;

    UpdateProgramsList;
    UpdateFileProperties;
    UpdateProgramProperties;

    cfile.Current  := 0;
    sgPrograms.Row := cfile.Current;
  end;

procedure TfmMain.acFileSaveAsExecute(Sender: TObject);
  begin
    DialogSaveFileDone;
  end;

procedure TfmMain.acFileSaveExecute(Sender: TObject);
  begin
    if not cfile.SaveToFile then
      DialogSaveFileDone;
    UpdateFileProperties;
  end;

procedure TfmMain.acFileCloseExecute(Sender: TObject);
  begin
    if cfile.Changed then
      case fmConfirm.Show(TXT_CONFIRM, TXT_FILECHANGED,
          [mbYes, mbNo, mbCancel], Self) of
        mrYes:
          if FileExistsUTF8(cfile.FileName) then
            cfile.SaveToFile else
            if not DialogSaveFileDone then Exit;
        mrNo: ;
        mrCancel: Exit;
        end;

    cfile.Create;
    UpdateProgramsList;
    UpdateFileProperties;
    UpdateProgramProperties;
  end;

procedure TfmMain.acFileRemoveExecute(Sender: TObject);
  begin
    FileRemove(fbFiles.FileName, fmSettings.RemoveToTrash);
    fbFiles.UpdateFileList;
  end;


{ ==== обработчики действий группы <Программы> / acProg[Action] ==== }

procedure TfmMain.acProgCopyExecute(Sender: TObject);
  begin
    cfile.ClipboardOperation(coCopy);
  end;

procedure TfmMain.acProgCutExecute(Sender: TObject);
  begin
    cfile.ClipboardOperation(coCut);
    UpdateProgramsList;
    UpdateProgramProperties;
  end;

procedure TfmMain.acProgPasteExecute(Sender: TObject);
  begin
    cfile.ClipboardOperation(coPaste);
    UpdateProgramsList;
    UpdateProgramProperties;
  end;

procedure TfmMain.acProgRemoveExecute(Sender: TObject);
  begin
    cfile.Remove();
    UpdateProgramsList;
    UpdateProgramProperties;
  end;

procedure TfmMain.acProgRemoveAllExecute(Sender: TObject);
  begin
    cfile.RemoveAll;
    UpdateProgramsList;
    UpdateProgramProperties;
  end;


{ ==== обработчики действий группы <Общее> ==== }

procedure TfmMain.acSettingsExecute(Sender: TObject);
  begin
    fmSettings.ShowModal;
    SettingsApply;
  end;

procedure TfmMain.acVisitSiteExecute(Sender: TObject);
  begin
    fmAbout.VisitSite;
  end;

procedure TfmMain.acAboutExecute(Sender: TObject);
  begin
    fmAbout.Show;
  end;

procedure TfmMain.acExitExecute(Sender: TObject);
  begin
    Close;
  end;



initialization
  comm  := TCasioCommThread.Create;
  cfile := TCasioFile.Create;


finalization
  comm.Terminate;
  FreeAndNil(cfile);

end.
