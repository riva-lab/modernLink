unit fm_port;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, ExtCtrls, StdCtrls, Buttons,
  IniPropStorage, LCLType, Controls, Dialogs,
  com_detect, u_strings, fm_confirm, fm_settings;

type

  { TfmPort }

  TfmPort = class(TForm)
    bbCancel:   TBitBtn;
    bbApply:    TBitBtn;
    bbHelp:     TBitBtn;
    bbScan:     TBitBtn;
    bvLine:     TBevel;
    cbPort:     TComboBox;
    cbBaud:     TComboBox;
    cbParity:   TComboBox;
    imIcon:     TImage;
    lbPort:     TLabel;
    lbBaudrate: TLabel;
    lbParity:   TLabel;
    lbPortHint: TLabel;
    pContent:   TPanel;
    pPort:      TPanel;
    pBaudrate:  TPanel;
    pParity:    TPanel;
    pButtons:   TPanel;

    procedure bbCancelClick(Sender: TObject);
    procedure bbScanClick(Sender: TObject);
    procedure cbParityChange(Sender: TObject);
    procedure FormClose(Sender: TObject; var CloseAction: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormUTF8KeyPress(Sender: TObject; var UTF8Key: TUTF8Char);
  PRIVATE
    FBaud:       Integer;
    FParity:     Char;
    FPort:       String;
    FPortPrev:   String;
    FBaudPrev:   String;
    FParityPrev: Integer;
    FParityCurr: Integer;
    FPortList:   TStringList;
    FPortsCount: Integer;
  PUBLIC
    procedure SaveSettingsToIni(AStorage: TIniPropStorage);
    procedure LoadSettingsFromIni(AStorage: TIniPropStorage);
    procedure UpdateParameters;

    property Port: String read FPort;
    property Baud: Integer read FBaud;
    property Parity: Char read FParity;
    property PortsCount: Integer read FPortsCount;
  end;

var
  fmPort: TfmPort;

implementation

{$R *.lfm}

{ TfmPort }


procedure TfmPort.FormCreate(Sender: TObject);
  begin
    FPortList := TStringList.Create;
  end;

procedure TfmPort.FormShow(Sender: TObject);
  begin
    bbScan.Click;
    bbCancel.SetFocus;

    bbScan.Constraints.MinWidth := bbScan.Height;
    bbScan.Constraints.MaxWidth := bbScan.Height;

    cbParity.Clear;
    cbParity.Items.Append(TXT_PARITY_NONE);
    cbParity.Items.Append(TXT_PARITY_EVEN);
    cbParity.Items.Append(TXT_PARITY_ODD);
    cbParity.ItemIndex := FParityCurr;

    FPortPrev   := cbPort.Text;
    FBaudPrev   := cbBaud.Text;
    FParityPrev := FParityCurr;

    lbPortHint.Caption := MultiString(HINT_PORT);
  end;

procedure TfmPort.FormClose(Sender: TObject; var CloseAction: TCloseAction);
  begin
    UpdateParameters;
  end;

procedure TfmPort.FormUTF8KeyPress(Sender: TObject; var UTF8Key: TUTF8Char);
  begin
    if UTF8Key = chr(27) then bbCancel.Click;
  end;


procedure TfmPort.bbScanClick(Sender: TObject);
  var
    tmp: String;
  begin
    tmp := cbPort.Text;
    DetectComPorts(FPortList, cbPort.Items, '', '  [' + TXT_PORT_BUSY + ']');

    FPortsCount := FPortList.Count;

    if fmPort.PortsCount > 0 then
      begin
      cbPort.ItemIndex := cbPort.Items.IndexOf(tmp);
      if cbPort.ItemIndex < 0 then
        cbPort.ItemIndex := 0;
      end
    else
      begin
      cbPort.Text := 'no ports';
      if Showing and fmSettings.ShowPortWarning then
        fmConfirm.Show(TXT_WARNING, MultiString(TXT_NOPORTS), [mbYes], Self);
      end;
  end;

procedure TfmPort.bbCancelClick(Sender: TObject);
  begin
    cbPort.Text := FPortPrev;
    cbBaud.Text := FBaudPrev;
    FParityCurr := FParityPrev;
  end;

procedure TfmPort.cbParityChange(Sender: TObject);
  begin
    FParityCurr := cbParity.ItemIndex;
  end;


procedure TfmPort.UpdateParameters;
  const
    p = 'NEO';
  begin
    if cbPort.ItemIndex < 0 then
      FPort := cbPort.Text else
      FPort := FPortList.Strings[cbPort.ItemIndex];

    if FParityCurr >= 0 then
      FParity := p[FParityCurr + 1];

    FBaud := StrToInt(cbBaud.Text);
  end;

procedure TfmPort.SaveSettingsToIni(AStorage: TIniPropStorage);
  begin
    AStorage.IniSection := 'Connection';

    AStorage.WriteString('Port', cbPort.Text);
    AStorage.WriteString('Baudrate', cbBaud.Text);
    AStorage.WriteInteger('Parity', FParityCurr);
  end;

procedure TfmPort.LoadSettingsFromIni(AStorage: TIniPropStorage);
  begin
    bbScan.Click;
    AStorage.IniSection := 'Connection';

    cbPort.Text := AStorage.ReadString('Port', 'no ports');
    cbBaud.Text := AStorage.ReadString('Baudrate', '9600');
    FParityCurr := AStorage.ReadInteger('Parity', 0);

    UpdateParameters;
  end;

end.

