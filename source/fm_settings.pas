unit fm_settings;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, LazUTF8, LazFileUtils, Forms, Controls, Graphics,
  Dialogs, ExtCtrls, StdCtrls, IniPropStorage, Buttons, ActnList, ComCtrls,
  LCLTranslator, fm_confirm, u_CasioFile, u_strings;

const
  LANGUAGE_DEFAULT = 'RU, Russian - Русский';
  LANGUAGES_DIR    = DirectorySeparator + 'lang';
  LANGUAGES_FILE   = LANGUAGES_DIR + DirectorySeparator + 'languages.ini';
  SETTINGS_FILE    = DirectorySeparator + 'settings.ini';

type

  { TfmSettings }

  TfmSettings = class(TForm)
    acFontView:         TAction;
    acFontEplorer:      TAction;
    acOK:               TAction;
    acCancel:           TAction;
    acHelp:             TAction;
    acReset:            TAction;
    acGenExample:       TAction;
    bbHelp:             TBitBtn;
    bbSettCancel:       TBitBtn;
    bbSettOK:           TBitBtn;
    bResetSettings:     TButton;
    bGenExample:        TButton;
    cbShowFormatError:  TCheckBox;
    cbRemoveToTrash:    TCheckBox;
    cbRxRewriteWarning: TCheckBox;
    cbShowMenu:         TCheckBox;
    cbShowSplash:       TCheckBox;
    cbShowCatalog:      TCheckBox;
    cbAutoConnect:      TCheckBox;
    cbPreviewWrap:      TCheckBox;
    cbShowPortWarning:  TCheckBox;
    gbSystem:           TGroupBox;
    gbView:             TGroupBox;
    imIcon:             TImage;
    Notebook1:          TNotebook;
    Page1:              TPage;
    Page2:              TPage;
    pButtons:           TPanel;
    IniStorageSettings: TIniPropStorage;
    pcSettings:         TPageControl;
    DlgSave:            TSaveDialog;
    SettActionList:     TActionList;
    bbFontView:         TBitBtn;
    bbFontExplorer:     TBitBtn;
    cbLanguage:         TComboBox;
    dlgFont:            TFontDialog;
    gbL12n:             TGroupBox;
    gbFontView:         TGroupBox;
    lbLang:             TLabel;
    lbFontView:         TLabel;
    lbFontExplorer:     TLabel;
    rgRxAsk:            TRadioGroup;
    tsView:             TTabSheet;
    tsSystem:           TTabSheet;
    procedure acCancelExecute(Sender: TObject);
    procedure acFontExecute(Sender: TObject);
    procedure acGenExampleExecute(Sender: TObject);
    procedure acHelpExecute(Sender: TObject);
    procedure acOKExecute(Sender: TObject);
    procedure acResetExecute(Sender: TObject);
    procedure cbLanguageChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure IniStorageSettingsRestore(Sender: TObject);

  PRIVATE
    FFontExplorer:     TFont;
    FFontView:         TFont;
    FLanguage:         String;
    FLanguageIndex:    Integer;
    FPortAutoConnect:  Boolean;
    FPreviewWrap:      Boolean;
    FRemoveToTrash:    Boolean;
    FRxRewriteWarning: Boolean;
    FShowFormatError:  Boolean;
    FShowMenu:         Boolean;
    FShowCatalog:      Boolean;
    FShowPortWarning:  Boolean;
    FShowSplash:       Boolean;

    procedure IniStorageLangLoad;
    procedure UpdateFontLabels;

  PUBLIC
    procedure LoadFieldsFromComponents;
    procedure LoadComponentsFromFields;

    property Language: String read FLanguage;
    property FontView: TFont read FFontView;
    property FontExplorer: TFont read FFontExplorer;
    property ShowMenu: Boolean read FShowMenu;
    property ShowSplash: Boolean read FShowSplash;
    property ShowCatalog: Boolean read FShowCatalog;
    property ShowFormatError: Boolean read FShowFormatError;
    property RemoveToTrash: Boolean read FRemoveToTrash;
    property RxRewriteWarning: Boolean read FRxRewriteWarning;
    property PortAutoConnect: Boolean read FPortAutoConnect;
    property PreviewWrap: Boolean read FPreviewWrap;
    property ShowPortWarning: Boolean read FShowPortWarning;
  end;

var
  fmSettings:     TfmSettings;
  IniStorageLang: TIniPropStorage;

implementation

{$R *.lfm}

{ TfmSettings }

procedure TfmSettings.FormCreate(Sender: TObject);
  begin
    FFontExplorer := TFont.Create;
    FFontView     := TFont.Create;

    pcSettings.ActivePageIndex     := 0;
    IniStorageSettings.IniFileName := ExtractFileDir(ParamStrUTF8(0)) + SETTINGS_FILE;

    IniStorageLangLoad;
  end;

procedure TfmSettings.FormShow(Sender: TObject);
  var
    i, tmp: Integer;
  begin
    if Sender <> nil then
      LoadComponentsFromFields;

    tmp      := pcSettings.ActivePageIndex;
    Position := poDefault;

    for i := 1 to pcSettings.PageCount do
      begin
      pcSettings.ActivePageIndex := i - 1;
      AutoSize := True;

      Constraints.MinWidth  := Width;
      Constraints.MinHeight := Height;

      AutoSize := False;
      end;

    pcSettings.ActivePageIndex := tmp;
    Position := poMainFormCenter;
  end;


procedure TfmSettings.LoadFieldsFromComponents;
  begin
    FFontView.Assign(lbFontView.Font);
    FFontExplorer.Assign(lbFontExplorer.Font);

    FLanguageIndex    := cbLanguage.ItemIndex;
    FLanguage         := LowerCase(Copy(cbLanguage.Text, 1, 2));
    FRemoveToTrash    := cbRemoveToTrash.Checked;
    FRxRewriteWarning := cbRxRewriteWarning.Checked;
    FShowFormatError  := cbShowFormatError.Checked;
    FShowMenu         := cbShowMenu.Checked;
    FShowCatalog      := cbShowCatalog.Checked;
    FShowSplash       := cbShowSplash.Checked;
    FPortAutoConnect  := cbAutoConnect.Checked;
    FPreviewWrap      := cbPreviewWrap.Checked;
    FShowPortWarning  := cbShowPortWarning.Checked;
  end;

procedure TfmSettings.LoadComponentsFromFields;
  begin
    lbFontView.Font.Assign(FFontView);
    lbFontExplorer.Font.Assign(FFontExplorer);
    UpdateFontLabels;

    cbLanguage.ItemIndex       := FLanguageIndex;
    cbRemoveToTrash.Checked    := FRemoveToTrash;
    cbRxRewriteWarning.Checked := FRxRewriteWarning;
    cbShowFormatError.Checked  := FShowFormatError;
    cbShowMenu.Checked         := FShowMenu;
    cbShowCatalog.Checked      := FShowCatalog;
    cbShowSplash.Checked       := FShowSplash;
    cbAutoConnect.Checked      := FPortAutoConnect;
    cbPreviewWrap.Checked      := FPreviewWrap;
    cbShowPortWarning.Checked  := FShowPortWarning;
  end;

procedure TfmSettings.UpdateFontLabels;
  begin
    with lbFontView do
      Caption := Font.Name + ', ' + Font.Size.ToString;

    with lbFontExplorer do
      Caption := Font.Name + ', ' + Font.Size.ToString;
  end;


procedure TfmSettings.IniStorageLangLoad;
  var
    i, cnt: Integer;
  begin
    cbLanguage.Clear;
    cbLanguage.Items.Append(LANGUAGE_DEFAULT);

    IniStorageLang := TIniPropStorage.Create(nil);
    with IniStorageLang do
      begin
      IniFileName := ExtractFileDir(ParamStrUTF8(0)) + LANGUAGES_FILE;
      Active      := True;
      IniSection  := 'Languages List';

      // если приложение не нашло файл со списком локализаций - создаем его
      if not FileExistsUTF8(IniFileName) then
        begin
        WriteInteger('Count', 1);
        WriteString('L-1', LANGUAGE_DEFAULT);
        end;

      // считываем список локализаций, кроме 1-го пункта (язык по умолчанию)
      cnt := ReadInteger('Count', 1);
      cbLanguage.ItemIndex := 0;

      if cnt > 1 then
        for i := 2 to cnt do
          cbLanguage.Items.Append(ReadString('L-' + i.ToString, ''));
      end;
  end;

procedure TfmSettings.IniStorageSettingsRestore(Sender: TObject);
  begin
    LoadFieldsFromComponents;
  end;


procedure TfmSettings.acOKExecute(Sender: TObject);
  begin
    LoadFieldsFromComponents;

    ModalResult := mrOk;
  end;

procedure TfmSettings.acCancelExecute(Sender: TObject);
  begin
    LoadComponentsFromFields;

    ModalResult := mrCancel;
  end;

procedure TfmSettings.acHelpExecute(Sender: TObject);
  begin
    // используется модуль u_help.pas
    //case pcSettings.ActivePageIndex of
    //  0:
    //    CustomHelp.ShowTopic(120);
    //  end;
  end;

procedure TfmSettings.acResetExecute(Sender: TObject);
  begin
    if fmConfirm.Show(TXT_RESET, WARN_RESET, [mbYes, mbNo], Self) = mrYes then
      begin
      IniStorageSettings.Active := False;
      Application.MainForm.Close;

      // восстанавливаем настройки - удаляем файл настроек
      if FileExistsUTF8(IniStorageSettings.IniFileName) then
        DeleteFileUTF8(IniStorageSettings.IniFileName);
      end;
  end;

procedure TfmSettings.acFontExecute(Sender: TObject);
  begin
    if Sender = acFontView then dlgFont.Font.Assign(lbFontView.Font);
    if Sender = acFontEplorer then dlgFont.Font.Assign(lbFontExplorer.Font);

    if Sender <> nil then
      if dlgFont.Execute then
        begin
        if Sender = acFontView then  lbFontView.Font.Assign(dlgFont.Font);
        if Sender = acFontEplorer then  lbFontExplorer.Font.Assign(dlgFont.Font);
        end;

    UpdateFontLabels;
  end;

procedure TfmSettings.acGenExampleExecute(Sender: TObject);
  begin
    dlgSave.FileName   := 'example.cas';
    dlgSave.InitialDir := ExtractFileDir(ParamStrUTF8(0));

    if dlgSave.Execute then
      with TCasioFile.Create do
        begin
        SaveExampleToCas(dlgSave.FileName);
        Free;
        end;
  end;

procedure TfmSettings.cbLanguageChange(Sender: TObject);
  begin
    // применяем язык интерфейса не выходя из настроек
    SetDefaultLang(LowerCase(Copy(cbLanguage.Text, 1, 2)),
      ExtractFileDir(ParamStrUTF8(0)) + LANGUAGES_DIR);

    // перерисовываем форму, чтобы более длинные метки полностью помещались
    FormShow(nil);

    // обновляем метки предпросмотра шрифтов
    UpdateFontLabels;
  end;

end.

