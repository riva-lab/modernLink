program modernLink;

{$mode objfpc}{$H+}

uses
 {$IFDEF UNIX} {$IFDEF UseCThreads}
  cthreads,
 {$ENDIF} {$ENDIF}
  //LCLTranslator, // перевод
  Interfaces, Forms, SysUtils, fm_main, fm_about, fm_confirm, fm_port,
  fm_settings;

{$R *.res}

begin
  Application.Title := 'modern-Link';

  RequireDerivedFormResource := True;
  Application.Initialize;
  Application.CreateForm(TfmMain, fmMain);
  Application.CreateForm(TfmAbout, fmAbout);
  Application.CreateForm(TfmConfirm, fmConfirm);
  Application.CreateForm(TfmPort, fmPort);
  Application.CreateForm(TfmSettings, fmSettings);
  Application.Run;
end.

