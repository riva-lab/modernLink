unit u_CasioFile;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, LazFileUtils, LCLType, Clipbrd, u_CasioFileTokens;

const
  APP_CLIPBOARD_ID_STRING = 'casio-modernLink';

  CASIO_PROGS         = 38;
  CASIO_NAME_SIZE     = 12;
  CASIO_HEADER_SIZE   = 40;
  CASIO_PRGTABLE_SIZE = 192;
  CASIO_ROWTABLE_SIZE = 5;

  CASIDX_SIGNATURE = 0;
  CASIDX_1_SIGNP   = 1;
  CASIDX_1_TFMODE  = 2;
  CASIDX_1_RSRV1   = 3;
  CASIDX_1_SIZEHI  = 4;
  CASIDX_1_SIZELO  = 5;
  CASIDX_1_MODE    = 6;
  CASIDX_1_RSRV2   = 7;
  CASIDX_1_CS      = CASIO_HEADER_SIZE - 1;
  CASIDX_2_SIZEHI  = 2;
  CASIDX_2_SIZELO  = 3;
  CASIDX_2_MODE    = 4;
  CASIDX_2_CS      = CASIO_PRGTABLE_SIZE - 1;

  CASCOM_REQUEST    = $16;  //
  CASCOM_PRESENT    = $13;  //
  CASCOM_CONFIRM    = $06;  //
  CASCOM_BADSUMM    = $2B;  //
  CASCOM_EXISTS     = $21;  //
  CASCOM_SIGNATURE  = $3A;  //
  CASCOM_PACKET_DSC = $50;  //
  CASCOM_ONEPROG    = $31;  //
  CASCOM_ALLPROG    = $5A;  //
  CASCOM_PROGEND    = $FF;  //

  CASCOM_SDATA_MASK     = $02; //
  CASCOM_SGRAPH_MASK    = $80; //
  CASCOM_PROGMODE_MASK  = $70; //
  CASCOM_PROGMODE_COMP  = $00; //
  CASCOM_PROGMODE_SD1   = $10; //
  CASCOM_PROGMODE_LR1   = $20; //
  CASCOM_PROGMODE_BASEN = $40; //

type
  TClipboardOperation       = (coCut, coCopy, coPaste);
  TCasioProgMode            = (pmComp  = 0, pmSD1, pmLR1, pmBaseN, pmError);
  TCasioSDataMode           = (dmNon  = 0, dmSto, dmError);
  TCasioSGraphMode          = (gmNon = 0, gmDraw, gmError);
  TCasioTransferMode        = (tmSingle, tmSet);

const
  TxtCasioProgMode: array [TCasioProgMode] of String =
    ('Comp', 'SD', 'Reg', 'Base-N', 'Error');

  TxtCasioSDataMode: array [TCasioSDataMode] of String =
    ('Non', 'Sto', 'Error');

  TxtCasioSGraphMode: array [TCasioSGraphMode] of String =
    ('Non', 'Draw', 'Error');

  MaskCasioProgMode: array [TCasioProgMode] of Byte =
    (0, $10, $20, $40, 0);

  MaskCasioSDataMode: array [TCasioSDataMode] of Byte =
    (0, $02, 0);

  MaskCasioSGraphMode: array [TCasioSGraphMode] of Byte =
    (0, $80, 0);

type
  TCasioProgramProperties = record
    Name:    String[CASIO_NAME_SIZE];
    Comment: String[16];
    Size:    Integer;
    Empty:   Boolean;
    Mode:    TCasioProgMode;
    SData:   TCasioSDataMode;
    SGraph:  TCasioSGraphMode;
  end;

  TCasioProgramDescription = record
    Info:       TCasioProgramProperties;
    SizeAll:    Integer;
    StartIndex: Integer;
  end;

  TCasioProgramDescriptionArray = array[0 .. CASIO_PROGS - 1] of TCasioProgramDescription;

  TCasioProgramData = array [0..CASIO_PROGS - 1] of TBytes;

  TFileFormat = file of Byte;

  {
    Класс TCasioFile
  }

  { TCasioFile }

  TCasioFile = class
  PRIVATE
    FIsSingle:      Boolean;
    FIsValid:       Boolean;
    FError:         Boolean;
    FChanged:       Boolean;
    FCurrent:       Integer;
    FItemsCount:    Integer;
    FSizeProgsData: Integer;
    FTransferMode:  TCasioTransferMode;
    FProgDesc:      TCasioProgramDescriptionArray;
    FProgData:      TCasioProgramData; // данные всех программ
    FFile:          TFileFormat;
    FFileName:      String;
    FFileType:      String;

  PRIVATE
    procedure InitProgDescArray;
    function Increment(var x: Integer; incr: Integer = 1): Integer;
    function CalcChecksum(ABuffer: TBytes; ASize: Integer = 0): Byte;
    function IsChecksumValid(ABuffer: TBytes; AChecksum: Byte; ASize: Integer = 0): Boolean;
    function GetSDataMode(AByte: Byte): TCasioSDataMode;
    function GetSGraphMode(AByte: Byte): TCasioSGraphMode;
    function GetProgramMode(AByte: Byte): TCasioProgMode;
    function GetProgramName(Index: Integer): String;
    function GetItemsSize: Integer;

    function CreatePacket1Header: TBytes;
    function CreatePacket2Table: TBytes;
    function CreatePacket3Data: TBytes;
    function GetPacket(Index: Integer): TBytes;
    procedure ParsePacket1Header(ABuffer: TBytes);
    procedure ParsePacket2Table(ABuffer: TBytes);
    procedure ParsePacket3Data(ABuffer: TBytes);
    procedure SetPacket(Index: Integer; AValue: TBytes);

    procedure SetTransferMode(AValue: TCasioTransferMode);
    procedure SetCurrent(AValue: Integer);
    procedure SetRaw(AValue: TBytes);
    function GetRaw: TBytes;

    function ClipboardRawData(var AByte: TBytes; toCB: Boolean): Boolean;
  PUBLIC

    constructor Create;
    destructor Destroy; OVERRIDE;

    function Info(Index: Integer = -1): TCasioProgramProperties;
    function Preview(Index: Integer; ACasiodocFont: Boolean): String;

    function IsClipboardFull: Boolean;
    procedure ClipboardOperation(AOperation: TClipboardOperation; Index: Integer = -1);
    procedure Remove(Index: Integer = -1);
    procedure RemoveAll;

    function LoadFromFile(AFileName: String): Boolean;
    function LoadFromCas(AFileName: String): Boolean;

    function SaveToFile(AFileName: String = ''): Boolean;
    function SaveToCas(AFileName: String = ''): Boolean;
    { TODO : добавить формат файла CAT }
    function SaveExampleToCas(AFileName: String = ''): Boolean;

  PUBLIC
    property FileName: String read FFileName;
    property FileType: String read FFileType;

    property IsValid: Boolean read FIsValid;
    property IsSingle: Boolean read FIsSingle;
    property Error: Boolean read FError;
    property Changed: Boolean read FChanged;

    property ItemsCount: Integer read FItemsCount;
    property ItemsSize: Integer read GetItemsSize;
    property SizeProgsData: Integer read FSizeProgsData;

    property Current: Integer read FCurrent write SetCurrent;
    property TransferMode: TCasioTransferMode read FTransferMode write SetTransferMode;
    property Packet[Index: Integer]: TBytes read GetPacket write SetPacket;
    property Raw: TBytes read GetRaw write SetRaw;
  end;


implementation

{  ***  Класс  TCasioFile  ***  }

procedure TCasioFile.InitProgDescArray;
  begin
    FIsValid := False;
    FillByte(FProgDesc[0], SizeOf(TCasioProgramDescriptionArray), 0);
  end;

function TCasioFile.Increment(var x: Integer; incr: Integer): Integer;
  begin
    x      += incr;
    Result := x;
  end;

function TCasioFile.CalcChecksum(ABuffer: TBytes; ASize: Integer): Byte;
  var
    i: Integer;
  begin
    Result := 0;
    if ASize = 0 then ASize := Length(ABuffer);

    for i := 1 to ASize - 2 do
      Result := Byte(Result + ABuffer[i]);

    Result := Byte(-Result);
  end;

function TCasioFile.IsChecksumValid(ABuffer: TBytes; AChecksum: Byte;
  ASize: Integer): Boolean;
  begin
    Result := CalcChecksum(ABuffer, ASize) = AChecksum;
  end;

function TCasioFile.GetSDataMode(AByte: Byte): TCasioSDataMode;
  begin
    if AByte and CASCOM_SDATA_MASK > 0 then
      Result := dmSto else
      Result := dmNon;
  end;

function TCasioFile.GetSGraphMode(AByte: Byte): TCasioSGraphMode;
  begin
    if AByte and CASCOM_SGRAPH_MASK > 0 then
      Result := gmDraw else
      Result := gmNon;
  end;

function TCasioFile.GetProgramMode(AByte: Byte): TCasioProgMode;
  begin
    Result := pmError;
    case AByte and CASCOM_PROGMODE_MASK of
      CASCOM_PROGMODE_COMP: Result  := pmComp;
      CASCOM_PROGMODE_SD1: Result   := pmSD1;
      CASCOM_PROGMODE_LR1: Result   := pmLR1;
      CASCOM_PROGMODE_BASEN: Result := pmBaseN;
      end;
  end;

function TCasioFile.GetProgramName(Index: Integer): String;
  begin
    case Index of
      0..9: Result   := Index.ToString;
      10..35: Result := chr(Index + Ord('A') - 10);
      36: Result     := 'r';
      37: Result     := 'theta';
      end;
  end;

function TCasioFile.GetItemsSize: Integer;
  var
    i: Integer;
  begin
    Result := FProgDesc[0].Info.Size - 1;

    if not FIsSingle then
      for i := 1 to CASIO_PROGS - 1 do
        Result += FProgDesc[i].Info.Size - 1;
  end;


function TCasioFile.CreatePacket1Header: TBytes;
  var
    btfmode, bmode: Byte;
    bsize, i:       Integer;
  begin
    bmode := 0;
    bsize := 2;

    if FTransferMode = tmSingle then
      begin
      btfmode := CASCOM_ONEPROG;

      with FProgDesc[FCurrent].Info do
        begin
        bsize += Size;
        bmode := MaskCasioProgMode[Mode];
        bmode := bmode or MaskCasioSDataMode[SData];
        bmode := bmode or MaskCasioSGraphMode[SGraph];
        end;
      end
    else
      begin
      btfmode := CASCOM_ALLPROG;

      for i := 0 to CASIO_PROGS - 1 do
        bsize += FProgDesc[i].Info.Size;
      end;

    SetLength(Result, CASIO_HEADER_SIZE);
    FillByte(Result[0], CASIO_HEADER_SIZE, $FF);

    Result[CASIDX_SIGNATURE] := CASCOM_SIGNATURE;
    Result[CASIDX_1_SIGNP]   := CASCOM_PACKET_DSC;
    Result[CASIDX_1_TFMODE]  := btfmode;
    Result[CASIDX_1_RSRV1]   := 0;
    Result[CASIDX_1_SIZEHI]  := (bsize div 256) and $FF;
    Result[CASIDX_1_SIZELO]  := bsize and $FF;
    Result[CASIDX_1_MODE]    := bmode;
    Result[CASIDX_1_RSRV2]   := 0;
    Result[CASIDX_1_CS]      := CalcChecksum(Result);
  end;

function TCasioFile.CreatePacket2Table: TBytes;
  var
    bmode:       Byte;
    bsize, i, j: Integer;
  begin
    if FTransferMode = tmSingle then
      Exit(CreatePacket3Data);

    SetLength(Result, CASIO_PRGTABLE_SIZE);
    FillByte(Result[0], CASIO_PRGTABLE_SIZE, 0);

    for i := 0 to CASIO_PROGS - 1 do
      begin
      with FProgDesc[i].Info do
        begin
        bsize := Size;
        bmode := MaskCasioProgMode[Mode];
        bmode := bmode or MaskCasioSDataMode[SData];
        bmode := bmode or MaskCasioSGraphMode[SGraph];
        end;

      j := CASIO_ROWTABLE_SIZE * i;

      Result[j + CASIDX_2_SIZEHI] := (bsize div 256) and $FF;
      Result[j + CASIDX_2_SIZELO] := bsize and $FF;
      Result[j + CASIDX_2_MODE]   := bmode;
      end;

    Result[CASIDX_SIGNATURE] := CASCOM_SIGNATURE;
    Result[CASIDX_2_CS]      := CalcChecksum(Result);
  end;

function TCasioFile.CreatePacket3Data: TBytes;
  var
    bsize, i, j, Count: Integer;

  begin
    Count := 0;
    bsize := 2;

    if FTransferMode = tmSingle then
      begin
      SetLength(Result, 2 + FProgDesc[FCurrent].Info.Size);

      for j := 0 to FProgDesc[FCurrent].Info.Size - 1 do
        Result[Increment(Count)] := FProgData[FCurrent, j];
      end
    else
      for i := 0 to CASIO_PROGS - 1 do
        begin
        SetLength(Result, Increment(bsize, FProgDesc[i].Info.Size));

        for j := 0 to FProgDesc[i].Info.Size - 1 do
          Result[Increment(Count)] := FProgData[i, j];
        end;

    Result[CASIDX_SIGNATURE] := CASCOM_SIGNATURE;
    Result[High(Result)]     := CalcChecksum(Result);
  end;

function TCasioFile.GetPacket(Index: Integer): TBytes;
  begin
    SetLength(Result, 0);

    if FIsValid then
      case Index of
        0: Result := CreatePacket1Header;
        1: Result := CreatePacket2Table;
        2: Result := CreatePacket3Data;
        end;
  end;


procedure TCasioFile.ParsePacket1Header(ABuffer: TBytes);
  begin
    FChanged := True;
    FError   := True;
    InitProgDescArray;
    if Length(ABuffer) <> CASIO_HEADER_SIZE then Exit;
    if not IsChecksumValid(ABuffer, ABuffer[High(ABuffer)]) then Exit;
    if not (ABuffer[CASIDX_1_TFMODE] in [CASCOM_ONEPROG, CASCOM_ALLPROG]) then Exit;
    FIsSingle := ABuffer[CASIDX_1_TFMODE] = CASCOM_ONEPROG;
    FError    := False;

    if FIsSingle then
      FTransferMode := tmSingle else
      FTransferMode := tmSet;

    if FIsSingle then
      FCurrent := 0;

    with FProgDesc[0] do
      begin
      StartIndex     := 0;
      Info.Name      := GetProgramName(0);
      Info.Mode      := GetProgramMode(ABuffer[CASIDX_1_MODE]);
      Info.SData     := GetSDataMode(ABuffer[CASIDX_1_MODE]);
      Info.SGraph    := GetSGraphMode(ABuffer[CASIDX_1_MODE]);
      Info.Size      := 256 * ABuffer[CASIDX_1_SIZEHI] + ABuffer[CASIDX_1_SIZELO] - 2;
      Info.Empty     := Info.Size = 3;
      SizeAll        := Info.Size;
      FSizeProgsData := SizeAll;
      end;
  end;

procedure TCasioFile.ParsePacket2Table(ABuffer: TBytes);
  var
    i, j, index: Integer;
  begin
    FError := True;
    if FIsSingle then
      begin
      ParsePacket3Data(ABuffer);
      Exit;
      end;

    if Length(ABuffer) <> CASIO_PRGTABLE_SIZE then Exit;
    if not IsChecksumValid(ABuffer, ABuffer[High(ABuffer)]) then Exit;
    FError := False;

    index := 0;
    for i := 0 to CASIO_PROGS - 1 do
      with FProgDesc[i] do
        begin
        j           := i * CASIO_ROWTABLE_SIZE;
        Info.Name   := GetProgramName(i);
        Info.Mode   := GetProgramMode(ABuffer[j + CASIDX_2_MODE]);
        Info.SData  := GetSDataMode(ABuffer[j + CASIDX_2_MODE]);
        Info.SGraph := GetSGraphMode(ABuffer[j + CASIDX_2_MODE]);
        Info.Size   := 256 * ABuffer[j + CASIDX_2_SIZEHI] + ABuffer[j + CASIDX_2_SIZELO];
        Info.Empty  := Info.Size = 1;
        StartIndex  := index;
        index       += Info.Size;
        end;
  end;

procedure TCasioFile.ParsePacket3Data(ABuffer: TBytes);
  var
    i, j: Integer;
  begin
    FError := True;

    if Length(ABuffer) <> FProgDesc[0].SizeAll + 2 then Exit;
    if not IsChecksumValid(ABuffer, ABuffer[High(ABuffer)]) then Exit;

      try
      for i := 0 to CASIO_PROGS - 1 do
        begin
        SetLength(FProgData[i], FProgDesc[i].Info.Size);
        for j := 1 to FProgDesc[i].Info.Size do
          FProgData[i, j - 1] := ABuffer[FProgDesc[i].StartIndex + j];

        FProgDesc[i].Info.Comment := GetCommentFromCasioBinary(FProgData[i]);
        end;

      finally
      FError   := False;
      FIsValid := True;
      end;
  end;

procedure TCasioFile.SetPacket(Index: Integer; AValue: TBytes);
  begin
    case Index of
      0: ParsePacket1Header(AValue);
      1: ParsePacket2Table(AValue);
      2: ParsePacket3Data(AValue);
      end;
  end;


procedure TCasioFile.SetTransferMode(AValue: TCasioTransferMode);
  begin
    if FIsSingle then
      FTransferMode := tmSingle else
      FTransferMode := AValue;
  end;

procedure TCasioFile.SetCurrent(AValue: Integer);
  begin
    if FIsSingle then
      begin
      FCurrent := 0;
      Exit;
      end;

    if AValue <> FCurrent then
      if AValue < CASIO_PROGS then
        if AValue >= 0 then
          FCurrent := AValue else
          FCurrent := 0 else
        FCurrent := CASIO_PROGS - 1;
  end;

procedure TCasioFile.SetRaw(AValue: TBytes);
  var
    shift: Integer;
  begin
    // декодирование файла формата Casio-CAS (fx-7700)
    Packet[0]   := Copy(AValue, 0, CASIO_HEADER_SIZE);
    shift       := CASIO_HEADER_SIZE;
    FItemsCount := 1;

    if FError then Exit;

    if not FIsSingle then
      begin
      Packet[1]   := Copy(AValue, shift, CASIO_PRGTABLE_SIZE);
      shift       += CASIO_PRGTABLE_SIZE;
      FItemsCount := CASIO_PROGS;
      end;

    if FError then Exit;

    Packet[2] := Copy(AValue, shift, FProgDesc[0].SizeAll + 2);
  end;

function TCasioFile.GetRaw: TBytes;

  procedure AddBytesToArrayEnd(var dest: TBytes; src: TBytes);
    var
      i, start: Integer;
    begin
      start := Length(dest);
      SetLength(dest, Length(dest) + Length(src));

      for i := start to High(dest) do
        dest[i] := src[i - start];
    end;

  begin
    SetLength(Result, 0);

    if FIsValid and not FError then
        try
        AddBytesToArrayEnd(Result, Packet[0]);

        if FTransferMode = tmSet then
          AddBytesToArrayEnd(Result, Packet[1]);

        AddBytesToArrayEnd(Result, Packet[2]);
        finally
        end;
  end;


function TCasioFile.Info(Index: Integer): TCasioProgramProperties;
  begin
    if Index < 0 then Index := FCurrent;
    if Index < CASIO_PROGS then
      Result := FProgDesc[Index].Info;
  end;

function TCasioFile.Preview(Index: Integer; ACasiodocFont: Boolean): String;
  begin
    if Index < 0 then Index := FCurrent;
    if FIsSingle then Index := 0;

    if ACasiodocFont then
      Result := DecodeCasioBinary(FProgData[Index], cdfCasFnt)
    else
      Result := DecodeCasioBinary(FProgData[Index], cdfTxt);

    //with FProgDesc[Index].Info do
    //  Result := 'Программа <' + Name + '> ' + TxtCasioProgMode[Mode] +
    //    ' ' + TxtCasioSDataMode[SData] + ' ' + TxtCasioSGraphMode[SGraph] + #10 +
    //    //' M:' + IntToSys(FRaw.table.prog[i].mode, 2, 8, '', '') + #10 +
    //    DecodeCasioBinary(FProgData[Index]);
  end;

function TCasioFile.ClipboardRawData(var AByte: TBytes; toCB: Boolean): Boolean;
  var
    i:       Integer;
    cformat: TClipboardFormat;
    stream:  TMemoryStream;
  begin
      try
      Result          := False;
      cformat         := Clipboard.FindFormatID(APP_CLIPBOARD_ID_STRING);
      stream          := TMemoryStream.Create;
      stream.Position := 0;

      if toCB then
        begin
        // запись массива в буфер обмена
        if Length(AByte) > 0 then
          begin
          if cformat = 0 then
            cformat := RegisterClipboardFormat(APP_CLIPBOARD_ID_STRING);

          for i := 0 to high(AByte) do
            stream.WriteByte(AByte[i]);

          Clipboard.AddFormat(cformat, stream);
          end;
        end
      else
        begin
        // считывание массива из буфера обмена

        SetLength(AByte, 0);
        if cformat <> 0 then // проверка б.о. на наличие данных
          if Clipboard.GetFormat(cformat, stream) then
            begin
            stream.Position := 0;
            SetLength(AByte, stream.Size);

            for i := 0 to High(AByte) do
              AByte[i] := stream.ReadByte;
            end;
        end;

      stream.Free;
      Result := Length(AByte) > 0;

      except
      Result := False;
      SetLength(AByte, 0);
      end;
  end;

function TCasioFile.IsClipboardFull: Boolean;
  begin
      try
      Result := Clipboard.FindFormatID(APP_CLIPBOARD_ID_STRING) <> 0;
      except
      Result := False;
      end;
  end;

procedure TCasioFile.ClipboardOperation(AOperation: TClipboardOperation;
  Index: Integer);
  var
    tmpArray: TBytes;
  begin
    if Index < 0 then Index := FCurrent;
    if Index >= CASIO_PROGS then Exit;

    if AOperation in [coCopy, coCut] then
      begin
      tmpArray := Copy(FProgData[Index], 0, Length(FProgData[Index]));

      SetLength(tmpArray, Length(tmpArray) + 3);
      tmpArray[High(tmpArray) - 2] := Byte(FProgDesc[Index].Info.Mode);
      tmpArray[High(tmpArray) - 1] := Byte(FProgDesc[Index].Info.SData);
      tmpArray[High(tmpArray) - 0] := Byte(FProgDesc[Index].Info.SGraph);

      ClipboardRawData(tmpArray, True);
      end;

    if AOperation = coCut then
      Remove(Index);

    if AOperation = coPaste then
      if ClipboardRawData(tmpArray, False) then
        begin
        FProgDesc[Index].Info.Mode   := TCasioProgMode(tmpArray[High(tmpArray) - 2]);
        FProgDesc[Index].Info.SData  := TCasioSDataMode(tmpArray[High(tmpArray) - 1]);
        FProgDesc[Index].Info.SGraph := TCasioSGraphMode(tmpArray[High(tmpArray) - 0]);
        FProgData[Index]             := Copy(tmpArray, 0, Length(tmpArray) - 3);
        FProgDesc[Index].Info.Size   := Length(FProgData[Index]);

        tmpArray := Raw;
        Raw      := copy(tmpArray, 0, Length(tmpArray));
        end;
  end;

procedure TCasioFile.Remove(Index: Integer);
  var
    tmpArray: TBytes;
  begin
    if Index < 0 then Index := FCurrent;
    if Index >= CASIO_PROGS then Exit;

    FProgDesc[Index].Info.Mode   := TCasioProgMode(0);
    FProgDesc[Index].Info.SData  := TCasioSDataMode(0);
    FProgDesc[Index].Info.SGraph := TCasioSGraphMode(0);

    SetLength(FProgData[Index], 1);
    FProgDesc[Index].Info.Size := 1;
    FProgData[Index, 0]        := CASCOM_PROGEND;

    tmpArray := Raw;
    Raw      := copy(tmpArray, 0, Length(tmpArray));
  end;

procedure TCasioFile.RemoveAll;
  var
    i: Integer;
  begin
    for i := 0 to CASIO_PROGS - 1 do
      Remove(i);
  end;


function TCasioFile.LoadFromFile(AFileName: String): Boolean;
  begin
    Result := False;

    if ExtractFileExt(LowerCase(AFileName)) = '.cas' then
      Result := LoadFromCas(AFileName);

    //if ExtractFileExt(LowerCase(AFileName)) = '.cat' then
    //  Result := LoadFromCat(AFileName);
    FChanged := False;
    if not Result then Create;
  end;

function TCasioFile.LoadFromCas(AFileName: String): Boolean;
  var
    size, i, shift: Integer;
    buffer:         TBytes;
  begin
    FFileType := 'CAS';
    FFileName := AFileName;

    if not FileExistsUTF8(FFileName) or
      (ExtractFileExt(LowerCase(FFileName)) <> '.cas') then
      Exit(False);

    size := FileSize(FFileName);
    if size <= CASIO_HEADER_SIZE then
      Exit(False);

    // считывание файла в массив байтов
      try
      Result := False;
      Assign(FFile, FFileName);
      Reset(FFile);

      SetLength(buffer, size);
      for i := 0 to High(buffer) do
        Read(FFile, buffer[i]);

      finally
      Close(FFile);
      end;

    Raw    := buffer; // декодирование файла формата Casio-CAS fx-7700
    Result := not FError and FIsValid;
  end;

function TCasioFile.SaveToFile(AFileName: String): Boolean;
  begin
    Result := False;
    if AFileName = '' then AFileName := FFileName;
    if AFileName = '' then Exit;

    if ExtractFileExt(LowerCase(AFileName)) = '.cas' then
      Result := SaveToCas(AFileName);

    //if ExtractFileExt(LowerCase(AFileName)) = '.cat' then
    //  Result := SaveToCas(AFileName);
  end;

function TCasioFile.SaveToCas(AFileName: String): Boolean;
  var
    i:      Integer;
    buffer: TBytes;

  begin
    Result := False;
    if AFileName = '' then AFileName := FFileName;
    if AFileName = '' then Exit;

    if FIsValid and not FError then
        try
        Assign(FFile, AFileName);
        Rewrite(FFile);

        buffer := Raw;
        for i := 0 to High(buffer) do
          Write(FFile, buffer[i]);

        finally
        Close(FFile);
        Result   := True;
        FChanged := False;
        end;
  end;

function TCasioFile.SaveExampleToCas(AFileName: String): Boolean;
  begin
    FIsSingle := True;
    FIsValid  := True;
    FError    := False;
    FCurrent  := 0;

    with FProgDesc[0] do
      begin
      StartIndex     := 0;
      Info.Name      := GetProgramName(0);
      Info.Mode      := TCasioProgMode(0);
      Info.SData     := TCasioSDataMode(0);
      Info.SGraph    := TCasioSGraphMode(0);
      Info.Empty     := False;
      Info.Size      := CasioBinaryExample(FProgData[0]);
      SizeAll        := Info.Size;
      FSizeProgsData := SizeAll;
      end;

    Result := SaveToCas(AFileName);
  end;


constructor TCasioFile.Create;
  begin
    FChanged       := False;
    FError         := True;
    FIsValid       := False;
    FCurrent       := 0;
    FItemsCount    := 0;
    FSizeProgsData := 0;
    FFileName      := '';
    FFileType      := '';
  end;

destructor TCasioFile.Destroy;
  begin
    inherited Destroy;
  end;


end.


