unit u_CasioFileTokens;

{
    В модуле содержится:
    - таблица декодирования байтов файла калькулятора Casio fx-7700
    - функция декодирования
    - функция извлечения комментария с начала программы
    - функция-генератор образцовой программы, содержащей все коды
}

{$mode objfpc}{$H+}

interface

uses
  SysUtils;

type
  TCasioDataFormat = (cdfEsc, cdfTxt, cdfCasFnt);

  TCasioToken = record
    code:   Byte;
    escape: String[20];
    txt:    String[20];
    tDos:   String[20];
    tWin:   String[20];
    tCDF:    String[20];
  end;

const
{
  Tokens outside the ASCII character set
  The Escape sequences are mainly for translation to CAT format
  https://casetta.tuxfamily.org/spec/newcat/token_list.0.2.1.txt
}
  Tokens: array[0..156] of TCasioToken = (
    (code: $01;  escape: '\fempto';          txt: 'fempto';           tDos: '';               tWin: '';         tCDF: ''#212),
    (code: $02;  escape: '\pico';            txt: 'pico';             tDos: '';               tWin: '';         tCDF: ''#211),
    (code: $03;  escape: '\nano';            txt: 'nano';             tDos: '';               tWin: '';         tCDF: ''#239),
    (code: $04;  escape: '\micro';           txt: 'micro';            tDos: '';               tWin: '';         tCDF: ''#181),
    (code: $05;  escape: '\milli';           txt: 'milli';            tDos: '';               tWin: '';         tCDF: ''#208),
    (code: $06;  escape: '\kilo';            txt: 'kilo';             tDos: '';               tWin: '';         tCDF: ''#213),
    (code: $07;  escape: '\Mega';            txt: 'Mega';             tDos: '';               tWin: '';         tCDF: ''#215),
    (code: $08;  escape: '\Giga';            txt: 'Giga';             tDos: '';               tWin: '';         tCDF: ''#216),
    (code: $09;  escape: '\Tera';            txt: 'Tera';             tDos: '';               tWin: '';         tCDF: ''#217),
    (code: $0A;  escape: '\Peta';            txt: 'Peta';             tDos: '';               tWin: '';         tCDF: ''#218),
    (code: $0B;  escape: '\Exa';             txt: 'Exa';              tDos: '';               tWin: '';         tCDF: ''#219),
    (code: $0C;  escape: '\Disp';            txt: ' Disp'#10;         tDos: '';               tWin: '';         tCDF: ''#166#10),
    (code: $0D;  escape: ''#10;              txt: ''#10;              tDos: '';               tWin: '';         tCDF: ''#138#10),
    (code: $0E;  escape: '\->';              txt: '->';               tDos: ''#26;            tWin: '-'#139;    tCDF: ''#202),
    (code: $0F;  escape: '\EE';              txt: 'E';                tDos: '';               tWin: '';         tCDF: ''#140),

    (code: $10;  escape: '\<=';              txt: '<=';               tDos: ''#243;           tWin: '';         tCDF: ''#178),
    (code: $11;  escape: '\<>';              txt: '<>';               tDos: ''#216;           tWin: '';         tCDF: ''#173),
    (code: $12;  escape: '\>=';              txt: '>=';               tDos: ''#242;           tWin: '';         tCDF: ''#179),
    (code: $13;  escape: '\=>';              txt: '=>';               tDos: '='#10;           tWin: '';         tCDF: ''#201),
    (code: $14;  escape: '\f1';              txt: 'f1';               tDos: '';               tWin: '';         tCDF: #196'1'),
    (code: $15;  escape: '\f2';              txt: 'f2';               tDos: '';               tWin: '';         tCDF: #196'2'),
    (code: $16;  escape: '\f3';              txt: 'f3';               tDos: '';               tWin: '';         tCDF: #196'3'),
    (code: $17;  escape: '\f4';              txt: 'f4';               tDos: '';               tWin: '';         tCDF: #196'4'),
    (code: $18;  escape: '\f5';              txt: 'f5';               tDos: '';               tWin: '';         tCDF: #196'5'),
    (code: $19;  escape: '\f6';              txt: 'f6';               tDos: '';               tWin: '';         tCDF: #196'6'),
    (code: $1A;  escape: '\hA';              txt: '{A}';              tDos: '';               tWin: '';         tCDF: ''#129),
    (code: $1B;  escape: '\hB';              txt: '{B}';              tDos: '';               tWin: '';         tCDF: ''#245),
    (code: $1C;  escape: '\hC ';             txt: '{C}';              tDos: '';               tWin: '';         tCDF: ''#130),
    (code: $1D;  escape: '\hD';              txt: '{D}';              tDos: '';               tWin: '';         tCDF: ''#235),
    (code: $1E;  escape: '\hE';              txt: '{E}';              tDos: '';               tWin: '';         tCDF: ''#171),
    (code: $1F;  escape: '\hF';              txt: '{F}';              tDos: '';               tWin: '';         tCDF: ''#236),

    (code: $2A;  escape: '';                 txt: '*';                tDos: '';               tWin: '';         tCDF: ''#137),
    (code: $2F;  escape: '';                 txt: '/';                tDos: '';               tWin: '';         tCDF: ''#214),

    (code: $3A;  escape: ':';                txt: ':';                tDos: '';               tWin: '';         tCDF: ':'),
    (code: $3C;  escape: '<';                txt: '<';                tDos: '';               tWin: '';         tCDF: '<'),
    (code: $3D;  escape: '=';                txt: '=';                tDos: '';               tWin: '';         tCDF: '='),
    (code: $3E;  escape: '>';                txt: '>';                tDos: '';               tWin: '';         tCDF: '>'),
    (code: $3F;  escape: '?';                txt: '?';                tDos: '';               tWin: '';         tCDF: '?'),

    (code: $80;  escape: '\Pol(';            txt: 'Pol(';             tDos: '';               tWin: '';         tCDF: 'Pol('),
    (code: $81;  escape: '\sin ';            txt: 'sin ';             tDos: '';               tWin: '';         tCDF: 'sin '),
    (code: $82;  escape: '\cos ';            txt: 'cos ';             tDos: '';               tWin: '';         tCDF: 'cos '),
    (code: $83;  escape: '\tan ';            txt: 'tan ';             tDos: '';               tWin: '';         tCDF: 'tan '),
    (code: $84;  escape: '\Hex>';            txt: 'h';                tDos: '';               tWin: '';         tCDF: ''#249),
    (code: $85;  escape: '\ln ';             txt: 'ln ';              tDos: '';               tWin: '';         tCDF: 'ln '),
    (code: $86;  escape: '\sqrt ';           txt: 'sqrt ';            tDos: ''#251;           tWin: '';         tCDF: ''#195),
    (code: $87;  escape: '\(-)';             txt: '(-)';              tDos: ''#170;           tWin: ''#175;     tCDF: ''#151),
    (code: $88;  escape: '\nPr';             txt: ' nPr ';            tDos: '';               tWin: '';         tCDF: ''#184),
    (code: $89;  escape: '+';                txt: '+';                tDos: '';               tWin: '';         tCDF: '+'),
    (code: $8A;  escape: '\xnor';            txt: ' xnor ';           tDos: '';               tWin: '';         tCDF: 'xnor'),
    (code: $8B;  escape: '\x^2';             txt: '^2';               tDos: ''#253;           tWin: ''#178;     tCDF: ''#170),
    (code: $8C;  escape: '\Dms';             txt: '`';                tDos: '';               tWin: '';         tCDF: ''#234),
    (code: $8D;  escape: '\Integral(';       txt: 'Integral(';        tDos: #244'(';          tWin: '';         tCDF: #186'('),
    (code: $8E;  escape: '\Mo';              txt: 'Mo';               tDos: '';               tWin: '';         tCDF: 'Mo'),
    (code: $8F;  escape: '\Sumx2';           txt: 'Sumx2 ';           tDos: #228'x'#253;      tWin: '';         tCDF: #183'x'#170),

    (code: $91;  escape: '\asin ';           txt: 'arc sin ';         tDos: '';               tWin: 'sin'#175#185' ';  tCDF: 'sin'#135' '),
    (code: $92;  escape: '\acos ';           txt: 'arc cos ';         tDos: '';               tWin: 'cos'#175#185' ';  tCDF: 'cos'#135' '),
    (code: $93;  escape: '\atan ';           txt: 'arc tan ';         tDos: '';               tWin: 'tan'#175#185' ';  tCDF: 'tan'#135' '),
    (code: $94;  escape: '\Dec>';            txt: 'd';                tDos: '';               tWin: '';         tCDF: ''#248),
    (code: $95;  escape: '\log ';            txt: 'log ';             tDos: '';               tWin: '';         tCDF: 'log '),
    (code: $96;  escape: '\curt';            txt: 'Cur ';             tDos: '';               tWin: '';         tCDF: ''#163#195),
    (code: $97;  escape: '\Abs ';            txt: 'Abs ';             tDos: '';               tWin: '';         tCDF: 'Abs '),
    (code: $98;  escape: '\nCr ';            txt: ' nCr ';            tDos: '';               tWin: '';         tCDF: ''#141),
    (code: $99;  escape: '-';                txt: '-';                tDos: '';               tWin: '';         tCDF: '-'),
    (code: $9A;  escape: '\xor';             txt: ' xor ';            tDos: '';               tWin: '';         tCDF: 'xor'),
    (code: $9B;  escape: '\x^-1';            txt: '^(-1)';            tDos: '';               tWin: ''#175#185; tCDF: ''#135),
    (code: $9C;  escape: '\Deg>';            txt: ' deg';             tDos: ''#248;           tWin: ''#176;     tCDF: ''#161),
    (code: $9E;  escape: '\Med';             txt: 'Med ';             tDos: '';               tWin: '';         tCDF: 'Med'),
    (code: $9F;  escape: '\Sumx';            txt: 'Sumx ';            tDos: #228'x';          tWin: '';         tCDF: #183'x'),

    (code: $A0;  escape: '\Rec(';            txt: 'Rec(';             tDos: '';               tWin: '';         tCDF: 'Rec('),
    (code: $A1;  escape: '\sinh ';           txt: 'sinh ';            tDos: '';               tWin: '';         tCDF: 'sinh '),
    (code: $A2;  escape: '\cosh ';           txt: 'cosh ';            tDos: '';               tWin: '';         tCDF: 'cosh '),
    (code: $A3;  escape: '\tanh ';           txt: 'tanh ';            tDos: '';               tWin: '';         tCDF: 'tanh '),
    (code: $A4;  escape: '\Oct>';            txt: 'o';                tDos: '';               tWin: '';         tCDF: ''#251),
    (code: $A5;  escape: '\e^x';             txt: 'e';                tDos: '';               tWin: '';         tCDF: ''#142),
    (code: $A6;  escape: '\Int ';            txt: 'Int ';             tDos: '';               tWin: '';         tCDF: 'Int '),
    (code: $A7;  escape: '\ Not';            txt: 'Not ';             tDos: '';               tWin: '';         tCDF: 'Not '),
    (code: $A8;  escape: '^';                txt: '^';                tDos: '';               tWin: '';         tCDF: '^'),
    (code: $A9;  escape: '*';                txt: '*';                tDos: '';               tWin: ''#215;     tCDF: '*'),
    (code: $AA;  escape: '\or';              txt: ' or ';             tDos: '';               tWin: '';         tCDF: 'or'),
    (code: $AB;  escape: '!';                txt: '!';                tDos: '';               tWin: '';         tCDF: ''#207),
    (code: $AC;  escape: '\Rad>';            txt: ' rad';             tDos: '';               tWin: '';         tCDF: ''#131),
    (code: $AD;  escape: '\minY';            txt: 'minY';             tDos: '';               tWin: '';         tCDF: #186'('),
    (code: $AE;  escape: '\minX';            txt: 'minX';             tDos: '';               tWin: '';         tCDF: 'Min'),
    (code: $AF;  escape: '\Cnt';             txt: 'n';                tDos: '';               tWin: '';         tCDF: ''#247),

    (code: $B1;  escape: '\asinh ';          txt: 'arc sinh ';        tDos: '';               tWin: 'sinh'#175#185' ';  tCDF: 'sinh'#135' '),
    (code: $B2;  escape: '\acosh ';          txt: 'arc cosh ';        tDos: '';               tWin: 'cosh'#175#185' ';  tCDF: 'cosh'#135' '),
    (code: $B3;  escape: '\atanh ';          txt: 'arc tanh ';        tDos: '';               tWin: 'tanh'#175#185' ';  tCDF: 'tanh'#135' '),
    (code: $B4;  escape: '\Bin>';            txt: 'b';                tDos: '';               tWin: '';                 tCDF: ''#250),
    (code: $B5;  escape: '\10^x';            txt: '(10)^';            tDos: '';               tWin: '';         tCDF: ''#148),
    (code: $B6;  escape: '\Frac ';           txt: 'Frac ';            tDos: '';               tWin: '';         tCDF: 'Frac '),
    (code: $B7;  escape: '\Neg ';            txt: 'Neg ';             tDos: '';               tWin: '';         tCDF: 'Neg '),
    (code: $B8;  escape: '\xrt';             txt: ' Root ';           tDos: '';               tWin: '';         tCDF: ''#133#195),
    (code: $B9;  escape: '/';                txt: '/';                tDos: ''#246;           tWin: ''#247;     tCDF: '/'),
    (code: $BA;  escape: '\and';             txt: ' and ';            tDos: '';               tWin: '';         tCDF: 'and'),
    (code: $BB;  escape: '\ab/c';            txt: '_';                tDos: ''#169;           tWin: '';         tCDF: ''#194),
    (code: $BC;  escape: '\Gra>';            txt: ' gra';             tDos: '';               tWin: '';         tCDF: ''#132),
    (code: $BD;  escape: '\maxY';            txt: 'maxY';             tDos: '';               tWin: '';         tCDF: #186'('),
    (code: $BE;  escape: '\maxX';            txt: 'maxX';             tDos: '';               tWin: '';         tCDF: 'Max'),
    (code: $BF;  escape: '\Sumy2';           txt: 'Sumy2 ';           tDos: #228'y'#253;      tWin: '';         tCDF: #183'y'#170),

    (code: $C0;  escape: '\Ans';             txt: 'Ans';              tDos: '';               tWin: '';         tCDF: 'Ans'),
    (code: $C1;  escape: '\Ran#';            txt: 'Ran#';             tDos: '';               tWin: '';         tCDF: 'Ran#'),
    (code: $C2;  escape: '\MeanX';           txt: 'Mx';               tDos: '';               tWin: '';         tCDF: ''#152),
    (code: $C3;  escape: '\MeanY';           txt: 'My';               tDos: '';               tWin: '';         tCDF: ''#153),
    (code: $C4;  escape: '\Sdx';             txt: 'SDx';              tDos: 'x'#229;          tWin: '';         tCDF: 'x'#238'n'),
    (code: $C5;  escape: '\Sdxn';            txt: 'SDxn';             tDos: 'x'#229'n';       tWin: '';         tCDF: 'x'#238'n'#136),
    (code: $C6;  escape: '\Sdy';             txt: 'SDy';              tDos: 'y'#229;          tWin: '';         tCDF: 'y'#238'n'),
    (code: $C7;  escape: '\Sdyn';            txt: 'SDyn';             tDos: 'y'#229'n';       tWin: '';         tCDF: 'y'#238'n'#136),
    (code: $C8;  escape: '\{a}';             txt: 'a';                tDos: '';               tWin: '';         tCDF: ''#232),
    (code: $C9;  escape: '\{b}';             txt: 'b';                tDos: '';               tWin: '';         tCDF: ''#233),
    (code: $CA;  escape: '\Cor';             txt: 'Cor';              tDos: '';               tWin: '';         tCDF: ''#228),
    (code: $CB;  escape: '\Eox';             txt: 'Ex';               tDos: '';               tWin: '';         tCDF: ''#154),
    (code: $CC;  escape: '\Eoy';             txt: 'Ey';               tDos: '';               tWin: '';         tCDF: ''#155),
    (code: $CD;  escape: '\r';               txt: 'r';                tDos: '';               tWin: '';         tCDF: ''#253),
    (code: $CE;  escape: '\theta';           txt: 'theta';            tDos: '';               tWin: '';         tCDF: ''#175),
    (code: $CF;  escape: '\Sumy';            txt: 'Sumy ';            tDos: #228'y';          tWin: '';         tCDF: #183'y'),

    (code: $D0;  escape: '\Pi';              txt: 'Pi';               tDos: ''#227;           tWin: '';         tCDF: ''#185),
    (code: $D1;  escape: '\Cls';             txt: 'Cls';              tDos: '';               tWin: '';         tCDF: 'Cls'),
    (code: $D2;  escape: '\@D2';             txt: 'Mcl';              tDos: '';               tWin: '';         tCDF: 'Mcl'),
    (code: $D3;  escape: '\Rnd';             txt: 'Rnd';              tDos: '';               tWin: '';         tCDF: 'Rnd'),
    (code: $D4;  escape: '\Dec';             txt: 'Dec';              tDos: '';               tWin: '';         tCDF: 'Dec'),
    (code: $D5;  escape: '\Hex';             txt: 'Hex';              tDos: '';               tWin: '';         tCDF: 'Hex'),
    (code: $D6;  escape: '\Bin';             txt: 'Bin';              tDos: '';               tWin: '';         tCDF: 'Bin'),
    (code: $D7;  escape: '\Oct';             txt: 'Oct';              tDos: '';               tWin: '';         tCDF: 'Oct'),
    (code: $D8;  escape: 'Scl';              txt: 'Scl';              tDos: '';               tWin: '';         tCDF: 'Scl'),
    (code: $D9;  escape: '\Norm';            txt: 'Norm';             tDos: '';               tWin: '';         tCDF: 'Norm'),
    (code: $DA;  escape: '\Deg';             txt: 'Deg';              tDos: '';               tWin: '';         tCDF: 'Deg'),
    (code: $DB;  escape: '\Rad';             txt: 'Rad';              tDos: '';               tWin: '';         tCDF: 'Rad'),
    (code: $DC;  escape: '\Gra';             txt: 'Grad';             tDos: '';               tWin: '';         tCDF: 'Gra'),
    (code: $DD;  escape: '\Eng';             txt: 'Eng ';             tDos: '';               tWin: '';         tCDF: 'Eng'),
    (code: $DE;  escape: '\Intg ';           txt: 'Intg ';            tDos: '';               tWin: '';         tCDF: 'Intg '),
    (code: $DF;  escape: '\Sumxy';           txt: 'Sumxy ';           tDos: #228'xy';         tWin: '';         tCDF: #183'xy'),

    (code: $E0;  escape: '\Plot ';           txt: 'Plot ';            tDos: '';               tWin: '';         tCDF: 'Plot '),
    (code: $E1;  escape: '\Line';            txt: 'Line';             tDos: '';               tWin: '';         tCDF: 'Line'),
    (code: $E2;  escape: '\Lbl ';            txt: 'Lbl ';             tDos: '';               tWin: '';         tCDF: 'Lbl '),
    (code: $E3;  escape: '\Fix ';            txt: 'Fix ';             tDos: '';               tWin: '';         tCDF: 'Fix '),
    (code: $E4;  escape: '\Sci ';            txt: 'Sci ';             tDos: '';               tWin: '';         tCDF: 'Sci '),
    (code: $E6;  escape: ' Cl';              txt: ' Cl';              tDos: '';               tWin: '';         tCDF: 'CL '),
    (code: $E7;  escape: ' Dt';              txt: ' Dt';              tDos: '';               tWin: '';         tCDF: 'DT '),
    (code: $E8;  escape: '\Dsz ';            txt: 'Dsz ';             tDos: '';               tWin: '';         tCDF: 'Dsz '),
    (code: $E9;  escape: '\Isz ';            txt: 'Isz ';             tDos: '';               tWin: '';         tCDF: 'Isz '),
    (code: $EA;  escape: '\Factor ';         txt: 'Factor ';          tDos: '';               tWin: '';         tCDF: 'Factor '),
    (code: $EB;  escape: '\ViewWindow ';     txt: 'Range ';           tDos: '';               tWin: '';         tCDF: 'Range '),
    (code: $EC;  escape: '\Goto';            txt: 'Goto ';            tDos: '';               tWin: '';         tCDF: 'Goto '),
    (code: $ED;  escape: '\Prog ';           txt: 'Prog ';            tDos: '';               tWin: '';         tCDF: 'Prog '),
    (code: $EE;  escape: '\Graph Y=';        txt: 'Graph_Y=';         tDos: '';               tWin: '';         tCDF: 'Graph Y='),
    (code: $EF;  escape: '\Graph Integral '; txt: 'Graph_Integral ';  tDos: 'Graph '#244' ';  tWin: '';         tCDF: 'Graph '#186),

    (code: $F0;  escape: '\Graph Y>';        txt: 'Graph Y>';         tDos: '';               tWin: '';         tCDF: 'Graph Y>'),
    (code: $F1;  escape: '\Graph Y<';        txt: 'Graph Y<';         tDos: '';               tWin: '';         tCDF: 'Graph Y<'),
    (code: $F2;  escape: '\Graph Y>=';       txt: 'Graph Y>=';        tDos: '';               tWin: '';         tCDF: 'Graph Y'#179),
    (code: $F3;  escape: '\Graph Y<=';       txt: 'Graph Y<=';        tDos: '';               tWin: '';         tCDF: 'Graph Y'#178),
    (code: $F4;  escape: '\Graph r=';        txt: 'Graph r=';         tDos: '';               tWin: '';         tCDF: 'Graph r='),
    (code: $F5;  escape: '\Graph (X,Y)=(';   txt: 'Graph (X,Y)=(';    tDos: '';               tWin: '';         tCDF: 'Graph(X,Y)=('),
    (code: $FB;  escape: '\ProbP(';          txt: 'ProbP(';           tDos: '';               tWin: '';         tCDF: 'P('),
    (code: $FC;  escape: '\ProbQ(';          txt: 'ProbQ(';           tDos: '';               tWin: '';         tCDF: 'Q('),
    (code: $FD;  escape: '\ProbR(';          txt: 'ProbR(';           tDos: '';               tWin: '';         tCDF: 'R('),
    (code: $FE;  escape: '\ProbT(';          txt: 'ProbT(';           tDos: '';               tWin: '';         tCDF: 't('),
    (code: $FF;  escape: '\EOF';             txt: '<EOF>'#10;         tDos: '';               tWin: '';         tCDF: #222'.EOF') );

    { ( code:$D2;  escape:'0\x0E' 'A~Z:0\x0E\xCD:0\x0E\xCE:26\x0E\x7F\x46\x7F\x51' '1' }
    { 0->A~Z:0->r:0->theta:26->Dim List 1 }
    { ( code:$D2;  escape:'\x7F\x2C' '0,X,1,26,1)\x0E\x7F\x51' '1:0\x0E'
            'A~Z:0\x0E\xCD:0\x0E\xCE'
            { Seq(0,X,1,26,1)->List 1:0->A~Z:0->r:0->theta } }


// функция декодирования данных программы CAS-файла
function DecodeCasioBinary(ARawData: TBytes; AFormat: TCasioDataFormat = cdfTxt): String;

// функция извлечения комментария с начала программы
function GetCommentFromCasioBinary(ARawData: TBytes): String;

// функция-генератор образцовой программы, содержащей все коды
function CasioBinaryExample(out AData: TBytes): Integer;


implementation

function DecodeCasioBinary(ARawData: TBytes; AFormat: TCasioDataFormat): String;
  var
    i, j: Integer;
  begin
    Result := '';

    if Length(ARawData) > 1 then
      for j := 0 to high(ARawData) - 1 do
        begin
        i := 0;
        while i < Length(Tokens) do
          begin
          if ARawData[j] = Tokens[i].code then
            begin
            if AFormat = cdfTxt then
              Result += Tokens[i].txt;

            if AFormat = cdfEsc then
              Result += Tokens[i].escape;

            if AFormat = cdfCasFnt then
              Result += Tokens[i].tCDF;

            break;
            end;

          if ARawData[j] = $FF then
            Exit;

          i += 1;
          end;

        if i = Length(Tokens) then
          Result += chr(ARawData[j]);
        end;
  end;

function GetCommentFromCasioBinary(ARawData: TBytes): String;
  var
    i, shift: Integer;
    m: Char;
  begin
    Result := '';
    shift  := 0;

    if Length(ARawData) < 3 then Exit;

    // поиск начала комментария или строки
    for shift := 0 to high(ARawData) do
      begin
      m := chr(ARawData[shift]);
      if m in ['''', '"']      then break;
      if ARawData[shift] = $FF then Exit;
      end;

    // считываем первые 16 символов комментария или строки
    // или до символа перехода на новую строку
    for i := shift + 1 to high(ARawData) do
      begin
      if i > 16                                 then Exit;
      if ARawData[i] = $FF                      then Exit;
      if (m = '"') and (chr(ARawData[i]) = '"') then Exit;
      if ARawData[i] in [$0C, $0D, Ord(':')]    then Exit;

      if ARawData[i] in [$20 .. $80] then Result += chr(ARawData[i]);
      if ARawData[i] = $87           then Result += '-';
      end;
  end;

function CasioBinaryExample(out AData: TBytes): Integer;
  var
    i, j: Integer;

  function PostInc(var x: Integer): Integer;
    begin
      Result := x;
      x      += 1;
    end;

  procedure PutCode(code: integer);
    begin
      AData[PostInc(Result)] := ord(IntToHex(i, 2)[1]);
      AData[PostInc(Result)] := ord(IntToHex(i, 2)[2]);
      AData[PostInc(Result)] := ord(' ');
      AData[PostInc(Result)] := i;
      AData[PostInc(Result)] := $D;
    end;

  begin
    Result := 0;
    SetLength(AData, 256*5);

    for i := 1 to 255 do
      if i < $7F then
        PutCode(i)
      else
        for j := 0 to High(Tokens) do
          if i = Tokens[j].code then
            begin
            PutCode(i);
            break;
            end;

    SetLength(AData, Result + 1);
  end;

end.

