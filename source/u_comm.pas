unit u_comm;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, synaser, u_CasioFile;

resourcestring
  TXT_CS_OFFLINE = 'Отключен';
  TXT_CS_ONLINE  = 'Готов';
  TXT_CS_RXING   = 'Прием...';
  TXT_CS_TXING   = 'Передача...';

  TXT_CE_NONE        = 'Успех';
  TXT_CE_RXERROR     = 'Ошибка приема';
  TXT_CE_TXNODEVICE  = 'Нет устройства';
  TXT_CE_TXERROR     = 'Ошибка передачи';
  TXT_CE_TXCANCEL    = 'Передача отменена';
  TXT_CE_DATAINVALID = 'Данные повреждены';

type
  TNewDataProc    = procedure of object;
  TNewRawDataProc = procedure(dataOne: Boolean) of object;
  TCasCommStatus  = (csOffline, csOnline, csRxing, csTxing);
  TCasCommError   = (ceNone, ceRxError, ceTxNoDevice, ceTxError, ceTxCancel, ceDataInvalid);

  ESerialReadTimeOut = class(Exception);


  {
    Класс TCasioCommThread
    Предназначен для связи с калькулятором Casio fx-7700
    через последовательный COM-порт.
    Работает в своем отдельном потоке для предотвращения
    зависания пользовательского интерфейса приложения.
  }

  { TCasioCommThread }

  TCasioCommThread = class(TThread)
  PRIVATE
    const
    TIMEOUT_PACKET = 2000;
    TIMEOUT_BYTE   = 100;
    TIME_DELAY     = 40;

  PRIVATE
    FSerial:        TBlockSerial;
    FCasioFile:     TCasioFile;
    FPort:          String;
    FPortBaud:      Integer;
    FPortParity:    Char;
    FStarted:       Boolean;
    FListen:        Boolean;
    FRunning:       Boolean;
    FConnected:     Boolean;
    FTxStart:       Boolean;
    FProgress:      Integer;
    FProgressMax:   Integer;
    FError:         TCasCommError;
    FStatus:        TCasCommStatus;
    FOnRxEnd:       TNewDataProc;
    FOnRxStart:     TNewDataProc;
    FOnTxEnd:       TNewDataProc;
    FOnTxStart:     TNewDataProc;
    FOnReceiving:   TNewDataProc;
    FOnTransmiting: TNewDataProc;

  PRIVATE
    FErrorString:  String;
    FStatusString: String;
    function GetErrorString: String;
    function GetProgressPercent: Integer;
    function GetStatusString: String;

    function SerialReadByte(ATimeout: Integer): Byte;
    function SerialReadBytes(var Data: TBytes; maxSize: Integer): Integer;
    procedure SerialWriteByte(AData: Byte);

    function PortOpened: Boolean;
    procedure ClosePort;
    function OpenPort: Boolean;
    function IsPortReady: Boolean;
    function IsPortReconnected: Boolean;

    function IsCommRequested: Boolean;
    function ReceivePacket(APacketN: Integer): Boolean;
    function ReceiveData: Boolean;

    function IsDevicePresent: Boolean;
    function DeviceAnswer: Byte;
    procedure TxBuffer(ABytes: TBytes);
    function TxPacket1Header: Byte;
    function TxPacket2Table: Byte;
    function TxPacket3Data: Byte;
    function TransmitData: Boolean;

    procedure Execute; OVERRIDE;

  PUBLIC
    constructor Create;
    destructor Destroy; OVERRIDE;

    procedure PortSettings(APort: String; ABaud: Integer; AParity: Char);

    procedure Transmit;
    procedure ResetError;

  PUBLIC
    property OnRxEnd: TNewDataProc read FOnRxEnd write FOnRxEnd;
    property OnRxStart: TNewDataProc read FOnRxStart write FOnRxStart;
    property OnTxEnd: TNewDataProc read FOnTxEnd write FOnTxEnd;
    property OnTxStart: TNewDataProc read FOnTxStart write FOnTxStart;
    property OnReceiving: TNewDataProc read FOnReceiving write FOnReceiving;
    property OnTransmiting: TNewDataProc read FOnTransmiting write FOnTransmiting;

    property Started: Boolean read FStarted write FStarted;
    property Listen: Boolean read FListen write FListen;
    property Progress: Integer read FProgress;
    property ProgressMax: Integer read FProgressMax;
    property ProgressPercent: Integer read GetProgressPercent;
    property Status: TCasCommStatus read FStatus;
    property Error: TCasCommError read FError;

    property CasioFile: TCasioFile read FCasioFile write FCasioFile;

    property Port: String read FPort;
    property Baudrate: Integer read FPortBaud;
    property Parity: Char read FPortParity;
    property Connected: Boolean read FConnected;

    property StatusString: String read GetStatusString;
    property ErrorString: String read GetErrorString;
  end;


implementation

{  ***  Класс  TCasioCommThread  ***  }

{ Методы доступа к свойствам класса get/set }

function TCasioCommThread.GetProgressPercent: Integer;
  begin
    if FProgressMax > 0 then
      Result := 100 * FProgress div FProgressMax else
      Result := 0;
  end;

function TCasioCommThread.GetErrorString: String;
  begin
    case FError of
      ceNone: Result        := TXT_CE_NONE;
      ceRxError: Result     := TXT_CE_RXERROR;
      ceTxNoDevice: Result  := TXT_CE_TXNODEVICE;
      ceTxError: Result     := TXT_CE_TXERROR;
      ceTxCancel: Result    := TXT_CE_TXCANCEL;
      ceDataInvalid: Result := TXT_CE_DATAINVALID;
      end;
  end;

function TCasioCommThread.GetStatusString: String;
  begin
    case FStatus of
      csOffline: Result := TXT_CS_OFFLINE;
      csOnline: Result  := TXT_CS_ONLINE;
      csRxing: Result   := TXT_CS_RXING;
      csTxing: Result   := TXT_CS_TXING;
      end;
  end;


{ Прием/передача данных через COM-порт }

function TCasioCommThread.SerialReadByte(ATimeout: Integer): Byte;
  begin
    Result := 0;

    if not Terminated then
        try
        if PortOpened then
          Result := FSerial.RecvByte(ATimeout);
        finally
        end;

    //if FSerial.LastError = ErrTimeout then
    //  raise ESerialReadTimeOut.Create('Serial Read TimeOut');
  end;

function TCasioCommThread.SerialReadBytes(var Data: TBytes; maxSize: Integer): Integer;
  var
    x: Byte;
  begin
    Result := 0;

    if not Terminated then
        try
        SetLength(Data, maxSize);

        if PortOpened then
          Data[0] := FSerial.RecvByte(TIMEOUT_PACKET);
        Result    := 1;

        while not Terminated and PortOpened do
          begin
          x := FSerial.RecvByte(TIMEOUT_BYTE);

          if (FSerial.LastError <> ErrTimeout) then
            begin
            Data[Result] := x;
            Result       += 1;
            end
          else
            break;

          if Result >= maxSize then
            break;

          FProgress += 1;
          Synchronize(FOnReceiving);
          end;

        finally
        SetLength(Data, Result);
        end;
  end;

procedure TCasioCommThread.SerialWriteByte(AData: Byte);
  begin
      try
      if not Terminated and Started then
        FSerial.SendByte(AData);
      finally
      end;
  end;


{ Управление подключением к COM-порту }

procedure TCasioCommThread.ClosePort;
  begin
    with FSerial do
      if PortOpened then
          try
          Flush;
          Purge;
          CloseSocket;
          finally
          //FPort := '-';
          end;
  end;

function TCasioCommThread.OpenPort: Boolean;
  begin
    with FSerial do
        try
        Connect(FPort);
        Config(FPortBaud, 8, FPortParity, SB1, False, False);
        Result := PortOpened;
        //FDevice.WriteLogTable('', '', '', 'Система', 'Порт',
        //  'Подключен к порту ' + APort);
        except
        ClosePort;
        Result := False;
        //FDevice.WriteLogTable('', '', '', 'Система', 'Порт',
        //  'Ошибка подключения к порту ' + APort);
        end;
  end;

function TCasioCommThread.PortOpened: Boolean;
  begin
      try
      Exit(FSerial.Handle <> system.THandle(-1));
      except
      Exit(False);
      end;
  end;

procedure TCasioCommThread.PortSettings(APort: String; ABaud: Integer; AParity: Char);
  var
    lastOpened: Boolean;

  function InRange(AValue, AMin, AMax: Variant): Boolean;
    begin
      Result := (AValue >= AMin) and (AValue <= AMax);
    end;

  begin
    lastOpened := PortOpened;
    ClosePort;

    if InRange(ABaud, 300, 3000000) then
      FPortBaud := ABaud;

    if AParity in ['N', 'E', 'O', 'M', 'S'] then
      FPortParity := AParity;

    if APort.StartsWith('COM') then
      begin
      while not (APort[APort.Length] in ['0'..'9']) do
        APort := APort.Remove(APort.Length - 1, 1);
      FPort   := APort;
      end
    else
      FPort := '---';

    if lastOpened then
      OpenPort;
  end;

function TCasioCommThread.IsPortReady: Boolean;
  begin
    Result := False;

    if not FStarted then
      ClosePort
    else
      if PortOpened then
        Result := True
      else
        Result := OpenPort;
  end;

function TCasioCommThread.IsPortReconnected: Boolean;
  begin
    Result := False;

    if PortOpened then
      begin
      ClosePort;
      Result := OpenPort;
      end;
  end;


{ Прием данных от калькулятора }

function TCasioCommThread.IsCommRequested: Boolean;
  begin
    Result := False;

    if SerialReadByte(10) = CASCOM_REQUEST then
      begin
      Sleep(TIME_DELAY);
      SerialWriteByte(CASCOM_PRESENT);
      Result := True;
      end;
  end;

function TCasioCommThread.ReceivePacket(APacketN: Integer): Boolean;
  var
    buffer: TBytes;
    size:   Integer;
  begin
    Result := False;

    case APacketN of
      0: size := CASIO_HEADER_SIZE;
      1: size := CASIO_PRGTABLE_SIZE;
      2: if FCasioFile.IsSingle then
          size := FCasioFile.Info(0).Size + 2 else
          size := FCasioFile.SizeProgsData + 2;
      end;

    SerialReadBytes(buffer, size);
    FCasioFile.Packet[APacketN] := buffer;

    Result := not FCasioFile.Error;
    if Result then
      SerialWriteByte(CASCOM_CONFIRM);
  end;

function TCasioCommThread.ReceiveData: Boolean;
  var
    err: TCasCommError;
  label
    exitLabel;
  begin
    if not IsCommRequested then Exit(False);

    Result       := False;
    FStatus      := csRxing;
    FProgressMax := 0;
    FProgress    := 0;

    Synchronize(FOnRxStart);
    FError := ceNone;
    err    := ceRxError;

    if not ReceivePacket(0) then goto exitLabel;

    FProgressMax := FCasioFile.Info(0).Size + CASIO_HEADER_SIZE - 2;

    if FCasioFile.IsSingle then
      begin
      if not ReceivePacket(2) then goto exitLabel;
      end
    else
      begin
      FProgressMax += CASIO_PRGTABLE_SIZE - 2;

      if not ReceivePacket(1) then goto exitLabel;
      if not ReceivePacket(2) then goto exitLabel;
      end;

    err    := ceNone;
    Result := True;

    exitLabel: ;
    Synchronize(FOnRxEnd);
    FStatus := csOnline;
    FError  := err;
  end;


{ Передача данных калькулятору }

procedure TCasioCommThread.Transmit;
  begin
    if not FCasioFile.IsValid then
      FError   := ceDataInvalid
    else
      FTxStart := FStarted and (FStatus = csOnline);
  end;

function TCasioCommThread.IsDevicePresent: Boolean;
  begin
    SerialWriteByte(CASCOM_REQUEST);
    Result := SerialReadByte(TIMEOUT_PACKET) = CASCOM_PRESENT;
  end;

function TCasioCommThread.DeviceAnswer: Byte;
  begin
    Result := SerialReadByte(TIMEOUT_PACKET);
    if FSerial.LastError = ErrTimeout then
      Result := 0;
  end;

procedure TCasioCommThread.TxBuffer(ABytes: TBytes);
  var
    i: Integer;
  begin
    for i := 0 to High(ABytes) do
      begin
      SerialWriteByte(ABytes[i]);

      FProgress += 1;
      Synchronize(FOnTransmiting);
      end;
  end;

function TCasioCommThread.TxPacket1Header: Byte;
  begin
    TxBuffer(FCasioFile.Packet[0]);
    Result := DeviceAnswer;

    if Result <> CASCOM_CONFIRM then
      if Result <> CASCOM_EXISTS then
        Result := Byte(-1)
      else
        begin
        Sleep(TIMEOUT_BYTE);
        SerialWriteByte(CASCOM_CONFIRM); // rewrite confirmation !!!
        Result := DeviceAnswer;

        if Result <> CASCOM_CONFIRM then
          Result := 0;
        end;
  end;

function TCasioCommThread.TxPacket2Table: Byte;
  begin
    TxBuffer(FCasioFile.Packet[1]);
    Result := DeviceAnswer;
  end;

function TCasioCommThread.TxPacket3Data: Byte;
  begin
    TxBuffer(FCasioFile.Packet[2]);
    Result := DeviceAnswer;
  end;

function TCasioCommThread.TransmitData: Boolean;
  var
    ans: Byte;
    err: TCasCommError;
  begin
    Result       := False;
    FTxStart     := False;
    FProgress    := 0;
    FProgressMax := Length(FCasioFile.Raw);
    FStatus      := csTxing;
    FError       := ceNone;
    err          := ceTxNoDevice;

    Synchronize(FOnTxStart);

    if IsDevicePresent then
      begin
      err := ceTxError;
      ans := TxPacket1Header;

      if ans = 0 then
        err := ceTxCancel;

      if ans = CASCOM_CONFIRM then
        begin
        if FCasioFile.TransferMode = tmSingle then
          Result := TxPacket3Data = CASCOM_CONFIRM
        else
          if TxPacket2Table = CASCOM_CONFIRM then
            Result := TxPacket3Data = CASCOM_CONFIRM;

        if Result then
          err := ceNone;
        end;
      end;

    FSerial.Purge;
    FError := err;

    if IsPortReconnected then
      FStatus := csOnline else
      FStatus := csOffline;

    Synchronize(FOnTxEnd);
  end;


procedure TCasioCommThread.ResetError;
  begin
    FError := ceNone;
  end;

procedure TCasioCommThread.Execute;
  var
    cf_prev: Boolean;
  begin
    FRunning := True;

    while not Terminated do
      begin

      FConnected := IsPortReady;

      if FConnected then
        begin
        FStatus := csOnline;

        if FListen then
          ReceiveData;

        if FTxStart then
          TransmitData;
        end
      else
        FStatus := csOffline;

      Sleep(TIMEOUT_BYTE div 2);
      end;

    ClosePort;
    FRunning := False;
    FStarted := False;
  end;


{ Конструктор / деструктор }

constructor TCasioCommThread.Create;
  begin
    FreeOnTerminate := False;

    FSerial    := TBlockSerial.Create;
    FCasioFile := TCasioFile.Create;
    FStarted   := False;
    FRunning   := False;
    FTxStart   := False;
    FConnected := False;
    FListen    := True;
    FStatus    := csOffline;
    FError     := ceNone;

    PortSettings('COM1', 9600, 'N');

    inherited Create(True);
  end;

destructor TCasioCommThread.Destroy;
  begin
    while FRunning do
      sleep(1);

    FreeAndNil(FSerial);
    FreeAndNil(FCasioFile);

    inherited Destroy;
  end;

end.
