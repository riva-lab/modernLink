unit u_help;

{$mode objfpc}{$H+}

interface

uses
  Classes, Windows, SysUtils;

type

  { THelpCustom }

  THelpCustom = class
  PRIVATE
    FHelpFile:   String;
    FHelpViewer: String;

  PUBLIC
    procedure ShowTopic(ATopicID: String);
    procedure ShowTopic(AContext: Integer);

    constructor Create;
    destructor Destroy; OVERRIDE;

  PUBLIC
    property HelpFile: String read FHelpFile write FHelpFile;
  end;

var
  CustomHelp: THelpCustom;

implementation

{ THelpCustom }

procedure THelpCustom.ShowTopic(ATopicID: String);
  begin
    if ExtractFileExt(ATopicID) = '' then ATopicID += '.html';
    WinExec(PChar(FHelpViewer + ' ' + FHelpFile + '::/' + ATopicID), SW_SHOW);
  end;

procedure THelpCustom.ShowTopic(AContext: Integer);
  begin
    if AContext > 0 then
      WinExec(PChar(FHelpViewer + ' -mapid ' + AContext.ToString + ' ' + FHelpFile),
        SW_SHOW);
  end;


constructor THelpCustom.Create;
  begin
    FHelpViewer := 'hh.exe';
    FHelpFile   := '';
  end;

destructor THelpCustom.Destroy;
  begin
    inherited Destroy;
  end;


initialization
  CustomHelp := THelpCustom.Create;

end.

